#!/bin/bash

driver=$1
dir=$2
major=`awk "{if(\\$2==\\"$driver\\") {print \\$1}}" /proc/devices`
mknod /dev/$dir/$driver c $major 0
