#include<linux/module.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include<asm/uaccess.h>
MODULE_LICENSE ("GPL");

int null_major = 0;
//open
	static int 
null_open(struct inode *inode,struct file *filp)
{printk("the null driver open\n");
	return 0;
}

//release
	static int
null_release(struct inode *inode,struct file *filp)
{printk("the null driver released\n");
	return 0;
}

//write
	static ssize_t
null_write (struct file *filp, const char __user * buf, size_t count,
		loff_t * pos)
{
	return count;
}

//read
	static ssize_t
null_read (struct file * filp, const char __user * buf, size_t count,
		loff_t * pos)
{
	return 0;
}

struct file_operations null_fops = {
	.open=null_open,
	.release=null_release,
	.read = null_read,
	.write = null_write,
	.owner = THIS_MODULE,
};

	int
null_init (void)
{
	if ((null_major = register_chrdev (0, "nulldev", &null_fops)) >= 0)
	{
		printk ("register success!\n");
		return 1;
	}
	else
	{
		printk ("register failed!\n");
		return 0;
	}
}

null_cleanup (void)
{
	unregister_chrdev (null_major, "nulldev");
}

module_init (null_init);
module_exit (null_cleanup);
