#include "my_cdev.h"

int my_init()
{
	my_null_init();
	my_zero_init();
	my_buff_init();
}

void my_exit()
{
	my_null_exit();
	my_zero_exit();
	my_buff_exit();
}

MODULE_AUTHOR("zy");
MODULE_LICENSE("GPL");

module_init(my_init);
module_exit(my_exit);