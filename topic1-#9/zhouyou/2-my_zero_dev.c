#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>


#define SUCCESS 0

//声明设备，这是本设备的名字，将会出现在/proc/devices
#define DEVICE_NAME "my_zero_dev"
 
// 在这个文件中，主设备号作为全局变量以便于这个设备在注册和释放的时候使用
static int Major;
static int Minor;

#ifndef MEMDEV_MAJOR
#define MEMDEV_MAJOR 251   //预设的mem的主设备号
#endif

static int my_major = MEMDEV_MAJOR;

struct cdev my_zero_dev;
/**********************************************************************/

//open()函数
//功能：无论一个进程何时试图去打开这个设备都会调用这个函数
static int my_zero_open(struct inode *inode, struct file *file)
{	
	Minor = MINOR(inode->i_rdev);
	Major = MAJOR(inode->i_rdev);
	
	//dev_t i_rdev; 设备文件的inode结构,该字段表示真正的设备编号
	printk("Open_Device my_zero: %d.%d\n", Major, Minor);
 
	return SUCCESS;
}

//release ( ) 函数
//功能： 当一个进程试图关闭这个设备特殊文件的时候调用这个函数
static int my_zero_release(struct inode *inode, struct file *file)
{
	printk("Release_Device my_zero: %d.%d\n", Major, Minor);
	return 0;
}

//read ( ) 函数
//功能：当一个进程已经打开此设备文件以后并且试图去读它的时候调用这个函数（把读出的数据放到这个缓冲区,缓冲区的长度,文件中的偏移）
// 读/dev/zero设备可以读任意个0，写入无反应
static ssize_t my_zero_read(struct file *file, char *buffer, size_t length, loff_t *offset) 
{
	printk("Read_Device my_zero: %d.%d\n", Major, Minor);
	printk("Read %d bytes '0'\n", length);
	
	int count = length;
	while(count--)
		put_user('\0',buffer++);

	return length;
}


//write ( )  函数
//功能：当试图将数据写入这个设备文件的时侯，这个函数被调用
static ssize_t my_zero_write(struct file *file, const char *buffer, size_t length, loff_t *offset) 	
{
	printk("Write_Device my_zero: %d.%d\n", Major, Minor);
	return length;
}

//这个设备驱动程序提供给文件系统的接口
//当一个进程试图对我们生成的设备进行操作的时候就利用下面这个结构，这个结构就是我们提供给操作系统的接口，它的指针保存在设备表中,在init_module（）中被传递给操作系统
 
struct file_operations my_zero_fops = {
	.owner = THIS_MODULE,
	.read = my_zero_read,
	.write = my_zero_write,
	.open = my_zero_open,
	.release = my_zero_release,
 };
 
 
//模块的初始化和模块的卸载
//这个函数用来初始化这个模块 —注册该字符设备
//init_module()函数调用module_register_chrdev，把设备驱动程序添加到内核的字符设备驱动程序表中，它返回这个驱动程序所使用的主设备号。
static int my_zero_init()
{
	int result;
	int err;
 
	dev_t my_zero = MKDEV(my_major, 0);
	
	result = register_chrdev_region(my_zero, 1, "my_zero_dev");
 
	//静态申请设备号
	if (result < 0)
	{
		result = alloc_chrdev_region(&my_zero, 0, 1, "my_zero_dev");
		my_major = MAJOR(my_zero);
	}  
   
	printk("alloc successfully my_zero:%d(MAJOR)\n", my_major);
	if (result < 0)
		return result;
	
	//初始化cdev结构
	cdev_init(&my_zero_dev, &my_zero_fops);        //使my_zero_dev与my_zero_fops联系起来
	my_zero_dev.owner = THIS_MODULE;               //owner成员表示谁拥有这个驱动程序，使“内核引用模块计数”加1；THIS_MODULE表示现在这个模块被内核使用，这是内核定义的一个宏
	my_zero_dev.ops = &my_zero_fops;
   
	//注册字符设备 
	err = cdev_add(&my_zero_dev, my_zero, 1);
	if(err)
		printk(KERN_NOTICE "Error %d adding my_zero",err);  
	printk("add successfully my_zero.\n");
	
	return 0;

}
 
 
//这个函数的功能是卸载模块，主要是从/proc中取消注册的设备特殊文件。
static void my_zero_exit()
{
	cdev_del(&my_zero_dev);                                        //注销设备
	unregister_chrdev_region(MKDEV(my_major,0),1);          //释放设备号
	printk("unregister successfully my_zero.\n");
}

MODULE_AUTHOR("zy");
MODULE_LICENSE("GPL");

module_init(my_zero_init);
module_exit(my_zero_exit);

