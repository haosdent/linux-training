#include<linux/init.h>
#include<linux/module.h>
#include<linux/types.h>
#include<linux/fs.h>
#include<linux/errno.h>
#include<linux/cdev.h>
#include<linux/kernel.h>
#include<linux/slab.h>
#include<linux/uaccess.h>

MODULE_LICENSE("GPL");

#ifndef MYCDEV_SIZE
#define MYCDEV_SIZE 4094
#endif
#ifndef MYCDEV_NUM
#define MYCDEV_NUM 3
#endif

static int my_major = 0;
module_param(my_major, int, 0);

static struct cdev simpleDevs[MYCDEV_NUM];

struct mem_dev {
	char *data;
	unsigned long size;
};
struct mem_dev *mem_devp;

int simple_open(struct inode *inode, struct file *filp)
{

	return 0;
}

int simple_release(struct inode *inode, struct file *filp)
{

	return 0;
}

static ssize_t null_read(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	int ret = -1;

	return ret;
}

static ssize_t null_write(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	int ret = 1;

	printk("write %d bytes.\n", size);
	return ret;
}

static ssize_t zero_read(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	int i=0;
	for (; i<size; i++) {
		buf[i++] = 0;
	}
	printk("read %d bytes.\n", size);
	return size;
}

static ssize_t zero_write(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	int ret = -1;

	return ret;
}

static ssize_t mycdev_read(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	int ret = 0;
	unsigned long p = *ppos;
	unsigned long count = size;
	struct mem_dev *dev = mem_devp;

	if (p >= MYCDEV_SIZE)
		return 0;
	if (count > MYCDEV_SIZE - p)
		count = MYCDEV_SIZE - p;
	if (copy_to_user(buf, (void*)(dev->data + p), count))
		ret = -EFAULT;
	else {
		*ppos += count;
		ret = count;
		printk(KERN_INFO "read %ld bytes from %ld.\n", count, p);
	}
	
	return ret;
}

static ssize_t mycdev_write(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	int ret = 0;
	unsigned long p = *ppos;
	unsigned long count = size;
	struct mem_dev *dev = mem_devp;

	if (p >= MYCDEV_SIZE)
		return 0;
	if (count > MYCDEV_SIZE - p)
		count = MYCDEV_SIZE - p;
	if (copy_from_user(dev->data + p, buf, count))
		ret = -EFAULT;
	else {
		*ppos += count;
		ret = count;
		printk(KERN_INFO "write %ld bytes to %ld.\n", count ,p);
	}
	printk("write %d bytes.\n", size);
	return ret;
}

static loff_t mycdev_llseek(struct file *filp, loff_t offset, int whence)
{
	loff_t newpos;
	switch (whence) {
		case 0:
			newpos = offset;
			break;
		case 1:
			newpos = filp->f_pos + offset;
			break;
		case 2:
			newpos = MYCDEV_SIZE - 1 + offset;
			break;
		default:
			return -EINVAL;
	}
	if ((newpos<0) || (newpos>MYCDEV_SIZE))
		return -EINVAL;
	
	filp->f_pos = newpos;
	return newpos;
}

static struct file_operations null_fops = {
	.owner		= THIS_MODULE,
	.open		= simple_open,
	.release	= simple_release,
	.read		= null_read,
	.write		= null_write,
};

static struct file_operations zero_fops = {
	.owner		= THIS_MODULE,
	.open 		= simple_open,
	.release	= simple_release,
	.read		= zero_read,
	.write		= zero_write,
};

static struct file_operations mycdev_fops = {
	.owner		= THIS_MODULE,
	.open		= simple_open,
	.release	= simple_release,
	.read		= mycdev_read,
	.write		= mycdev_write,
	.llseek		= mycdev_llseek,
};

static void setup_cdev(struct cdev *dev, int minor, struct file_operations *fops)
{
	int err, devno;
	devno = MKDEV(my_major, minor);
	cdev_init(dev, fops);
	dev->owner = THIS_MODULE;
	dev->ops = fops;
	err = cdev_add(dev, devno, 1);
	if (err) {
		printk("taskdev: error %d adding taskdev.\n", err);
	}	
}

static int my_init(void)
{
	int ret;
	dev_t dev = MKDEV(my_major, 0);

	if (my_major)
		ret = register_chrdev_region(dev, 3, "task");
	else {
		ret = alloc_chrdev_region(&dev, 0, 3, "task");
		my_major = MAJOR(dev);
	}
	if (ret < 0) {
		printk("task: unable to get major %d.\n", my_major);
		return ret;
	}
	
	//devno = MKDEV(my_major, 0);
	/*cdev_init(&nullDev, &fops);
	nullDev.owner = THIS_MODULE;
	nullDev.ops = &fops;
	ret = cdev_add(&nullDev, devno, 1);
	if (ret) {
		printk("null: error %d adding null.\n", ret);
	}
	*/
	setup_cdev(simpleDevs, 0, &null_fops);
	setup_cdev(simpleDevs + 1, 1, &zero_fops);
	setup_cdev(simpleDevs + 2, 2, &mycdev_fops);
	
	mem_devp = kmalloc(sizeof(struct mem_dev), GFP_KERNEL);
	if (!mem_devp) {
		ret = -ENOMEM;
		goto fail_malloc;
	}
	memset(mem_devp, 0, sizeof(struct mem_dev));
	/*make error 
	mem_devp.size = MYCDEV_SIZE;
	mem_devp.data = kmalloc(MYCDEV_SIZE, GFP_KERNEL);
	memset(mem_devp.data, 0, MYCDEV_SIZE);	
	*/
	mem_devp[0].size = MYCDEV_SIZE;
	mem_devp[0].data = kmalloc(MYCDEV_SIZE, GFP_KERNEL);
	memset(mem_devp[0].data, 0, MYCDEV_SIZE);
	
	return 0;
	
	fail_malloc:
	unregister_chrdev_region(MKDEV(my_major, 0), 3);
	return ret;
}

static void my_exit(void)
{
	cdev_del(simpleDevs);
	cdev_del(simpleDevs + 1);
	cdev_del(simpleDevs + 2);
	kfree(mem_devp);
	unregister_chrdev_region(MKDEV(my_major, 0), 3);
}

module_init(my_init);
module_exit(my_exit);