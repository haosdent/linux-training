#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/slab.h>

#define P_DEBUG(fmt, args...) printk("<1>" "<kernel>[%s]"fmt, __FUNCTION__, ##args)
#define SizeofBuf 4000
dev_t devno_b;
typedef struct b_cdev
{
	struct cdev cdev;
	char buffer[SizeofBuf];
} mybuff_cdev;
static struct b_cdev *dev;
//===============================================================
int myopen(struct inode *node, struct file *filp)
{
	printk("test: buffer open\n");
	return 0;
}
int myclose(struct inode *node, struct file *filp)
{
	printk("test: buffer close\n");
	return 0;
}
//===============================

static ssize_t mb_read(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
	char *p = dev->buffer;
	if( *offset >= SizeofBuf ) 
	{
		printk("test: buffer full\n");
		return 0;
	}
	
 	if(count > (SizeofBuf - *offset))
	{
  		printk("test: count too large\n");
		return 0;
	}
	
	memcpy( buf, p + *offset, count); 
	*offset += count;
	return count;
}

ssize_t mb_write(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
	char *p = dev->buffer;
	if( *offset >= SizeofBuf ) 
	{
		printk("test: buffer full\n");
		return 0;
	}
	
 	if( count > (SizeofBuf - *offset) )
	{
  		printk("test: count too large\n");
		return 0;
	}

	memcpy( p + *offset, buf, count) ;
	*offset += count;
	return count;
}

//======================================================================

loff_t mb_llseek (struct file *filp, loff_t offset, int whence)
{
	
        loff_t newpos;
	newpos = filp->f_pos;
        switch(whence)
        {
        	case 0: /* SEEK_SET */
                {	
			newpos = offset;
                	break;
		}
        	case 1: /* SEEK_CUR */
                {	
			newpos += offset;
                	break;
		}
        	default: /* can't happen */
			return -1;
        }
        if ( newpos < 0 || newpos > SizeofBuf )
                return -1;
        filp->f_pos = newpos;
        return newpos;
}






struct file_operations mybuff_fops=
{
	.write = mb_write,
	.read = mb_read,
	.open = myopen,
	.release = myclose,
	.llseek = mb_llseek
	//.owner = THIS_MODULE
};

static int __init my_init(void)
{
	int res;
	res = alloc_chrdev_region(&devno_b, 0, 1, "mybuff");
	if(res < 0){
		P_DEBUG("register devno_n errno!\n");
		return res;
	}
	printk("devno_n allocated\n");
	dev = (struct b_cdev *)kmalloc(sizeof(struct b_cdev), GFP_KERNEL);
	if(!dev){
		P_DEBUG("Can't alloc enough space!\n");
		return -1;
	}
	cdev_init( &(dev -> cdev), &mybuff_fops);
	(dev->cdev).owner = THIS_MODULE;
	res = cdev_add( &(dev->cdev), devno_b, 1);
	if(res < 0){
		P_DEBUG("cdev_add errno!\n");
		unregister_chrdev_region(devno_b, 1);
		return res;
	}

	return 0;


}
static void __exit my_exit(void)
{
 	cdev_del(&(dev->cdev));
	unregister_chrdev_region(devno_b, 1);
	printk("test: done\n");

	return;
}

module_init(my_init);
module_exit(my_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Chi Zhang");
MODULE_VERSION("1.0");

