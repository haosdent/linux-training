#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <asm/uaccess.h>

#define P_DEBUG(fmt, args...) printk("<1>" "<kernel>[%s]"fmt, __FUNCTION__, ##args)

  //  static char kbuf[4000];

//===============================================================
int myopenz(struct inode *node, struct file *filp)
{
	printk("test: zero open\n");
	return 0;
}
int myclosez(struct inode *node, struct file *filp)
{
	printk("test: zero close\n");
	return 0;
}
//===============================

static ssize_t mz_read(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
	//return count;
	//char zero = '\0';
	//char *zero_p = &zero;
	long int countk;

	printk("test: zero read\n");
	countk = (long int) count;
	printk("test: countk = %ld", countk);
	//while (count-- != 0) *buf++ = '0';
	//printk("test: %d\n", countk);
	//return 0;
	memset(buf, '\0', countk);
	//return count;
	//while (countk--) copy_to_user(buf + countk - 1, zero_p, 1);
	return count;
}

ssize_t mz_write(struct file *filp, char __user *buf, size_t count, loff_t *offset)
{
	printk("test: zero write\n");
	return count;
}

//======================================================================


dev_t devno_z;
struct cdev myzero_cdev;


struct file_operations myzero_fops=
{
	.write = mz_write,
	.read = mz_read,
	.open = myopenz,
	.release = myclosez,
	.owner = THIS_MODULE
};

static int __init my_init(void)
{
	int res;
/*	printk("\n\n\ntest: my init\n");
	if ((zero_major = register_chrdev (0, "zerodev", &mynull_fops)) >= 0) {
		printk ("register success!\n");
		return 1;
	} else {
		printk ("register failed!\n");
		return 0;
	}
*/


/*

	res = alloc_chrdev_region(&devno_n, 0, 1, "mynull");
	if(res < 0){
		P_DEBUG("register devno_n errno!\n");
		return res;
	}

	cdev_init(&mynull_cdev, &mynull_fops);
	mynull_cdev.owner = THIS_MODULE;
	res = cdev_add(&mynull_cdev, devno_n, 1);
	if(res < 0){
		P_DEBUG("cdev_add errno!\n");
		unregister_chrdev_region(devno_n, 1);
		return res;
	}

	printk("test: devno_n allocated\n");


*/
	//=======================================================
	res = alloc_chrdev_region(&devno_z, 0, 1, "myzero");
	if(res < 0){
		P_DEBUG("register devno_z errno!\n");
		return res;
	}
	printk("devno_z allocated\n");
	cdev_init(&myzero_cdev, &myzero_fops);
	myzero_cdev.owner = THIS_MODULE;
	res = cdev_add(&myzero_cdev, devno_z, 1);
	if(res < 0){
		P_DEBUG("cdev_add errno!\n");
		unregister_chrdev_region(devno_z, 1);
		return res;
	}
	//======================================================

/*
	res = alloc_chrdev_region(&devno_b, 0, 1, "mybuff");
	if(res < 0){
		P_DEBUG("register devno_b errno!\n");
		return res;
	}
	printk("devno_b allocated\n");
	cdev_init(&mybuff_cdev, &mybuff_fops);
	mybuff_cdev.owner = THIS_MODULE;
	res = cdev_add(&mybuff_cdev, devno_b, 1);
	if(res < 0){
		P_DEBUG("cdev_add errno!\n");
		unregister_chrdev_region(devno_b, 1);
		return res;
	}
*/
	return 0;


}
static void __exit my_exit(void)
{
 // unregister_chrdev (zero_major, "zerodev");
 // return;
/*	cdev_del(&mynull_cdev);
	unregister_chrdev_region(devno_n, 1);
	printk("test: done\n");
*/


	cdev_del(&myzero_cdev);
	unregister_chrdev_region(devno_z, 1);
	printk("test: done\n");
/*
	cdev_del(&mybuff_cdev);
	unregister_chrdev_region(devno_b, 1);
	printk("test: done\n");
*/
	return;
}

module_init(my_init);
module_exit(my_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Chi Zhang");
MODULE_VERSION("1.0");

