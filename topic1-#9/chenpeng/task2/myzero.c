#include <linux/fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>

#define MYZERO_MAJOR 150
#define CDEV_NAME "myzero"

MODULE_AUTHOR("Chen Peng");
MODULE_LICENSE("Dual BSD/GPL");

static int myzero_major = MYZERO_MAJOR;
static struct cdev myzero_cdev;

static int open_zero(struct inode *inode, struct file *filp)
{
	printk("myzero opened!\n");
	return 0;
}

static int release_zero(struct inode *innode, struct file *filp)
{
	printk("myzero released!\n");
	return 0;
}

static loff_t lseek_zero(struct file *filp, loff_t offset, int orig)
{
	printk("myzero lseek!\n");
	return filp->f_pos = 0;
}

static ssize_t read_zero(struct file *filp, char __user *buf, size_t count, loff_t *ppos)
{
	int i;
	int num = 0x0;
	for(i = 0; i < count; i++)
		copy_to_user(buf + i, &num, sizeof(char));
	printk("myzero read!\n");
	return count;
}	

static ssize_t write_zero(struct file *filp, const char __user *buf, size_t count, loff_t *ppos)
{
	printk("myzero write!\n");
	return count;
}

static struct file_operations myzero_fops = {
	.owner = THIS_MODULE,
	.open = open_zero,
	.release = release_zero,
	.llseek = lseek_zero,
	.read = read_zero,
	.write = write_zero,
};

static int __init myzero_init(void)
{
	int result;
	dev_t devno;
	/*
	if(myzero_major){
		devno = MKDEV(myzero_major, 0)
		result = register_chrdev_region(devno, 1, CDEV_NAME);
	}
	else{
		result = alloc_chrdev_region(&devno, 0, 1, CDEV_NAME);
		myzero_major = MAJOR(devno);
	}
	if(result < 0){
		printk("Can't get major devno:%d \n",myzero_major);
		return result;
	}*/
	devno = MKDEV(myzero_major, 0);
	result = register_chrdev_region(devno, 1, CDEV_NAME);
	if(result < 0){
		result = alloc_chrdev_region(&devno, 0, 1, CDEV_NAME);
		myzero_major = MAJOR(devno);
	}
	if(myzero_major < 0){
		printk("Cann't get major devno!\n");
		return myzero_major;
	}
	else
		printk("The major devno of myzero is %d\n", myzero_major);
		
	cdev_init(&myzero_cdev, &myzero_fops);
	myzero_cdev.owner = THIS_MODULE;
	myzero_cdev.ops = &myzero_fops;
	cdev_add(&myzero_cdev, MKDEV(myzero_major, 0), 1);
	printk("myzero_init!\n");
	return result;
}

static void __exit myzero_exit(void)
{
	cdev_del(&myzero_cdev);
	unregister_chrdev_region(MKDEV(myzero_major, 0), 1);
	printk("myzero exit!\n");
}

module_init(myzero_init);
module_exit(myzero_exit);
