#ifndef MYTMP_H
#define MYTMP_H
#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>

#define MYTMP_MAJOR 0
#define TMP_DEV_NAME "mytmp"

#define TMP_SIZE 0x1000

int __init mytmp_init(void);
void __exit mytmp_exit(void);

#endif