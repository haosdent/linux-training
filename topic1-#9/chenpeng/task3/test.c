#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

int main()
{
	int fd;
	char s[15] = "helloworld";
	char out[15]="";
	int len = strlen(s);
	int i;
	fd = open("/dev/mytmp", O_RDWR, S_IRUSR|S_IWUSR);
	if(fd != -1){
		lseek(fd, 0, SEEK_SET);
		i = write(fd, s, len);
		if(len == i)
			printf("write success\n");
		lseek(fd, 0, SEEK_END);
		lseek(fd, -4091, SEEK_END);
		i = read(fd, out, len);
		if(len == i){
			printf("read success\n");
			printf("read:%s\n",out);
		}
		else
			printf("read error\n");
		close(fd);
	}
	else
		printf("device cannot open\n");
	return 0;
}