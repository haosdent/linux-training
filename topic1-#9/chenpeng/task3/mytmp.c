#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>

#define MYTMP_MAJOR 0
#define CDEV_NAME "mytmp"

#define TMP_SIZE 0x1000

MODULE_AUTHOR("Chen Peng");
MODULE_LICENSE("Dual BSD/GPL");

static int mytmp_major = MYTMP_MAJOR;

typedef struct mytmp_cdev{
	struct cdev cdev;
	char tmp[TMP_SIZE];
}mytmp_cdev;
static struct mytmp_cdev *dev;

static int open_tmp(struct inode *inode, struct file *filp)
{
	printk("mytmp opened!\n");
	return 0;
}

static int release_tmp(struct inode *innode, struct file *filp)
{
	printk("mytmp released!\n");
	return 0;
}

static loff_t lseek_tmp(struct file *filp, loff_t offset, int whence)
{
	loff_t newpos;
	switch(whence){
	case 0: /*SEEK_SET*/
		newpos = offset;
		break;
	case 1: /*SEEK_CUR*/
		newpos = filp->f_pos + offset;
		break;
	case 2: /*SEEK_END*/
		newpos = TMP_SIZE + offset;
		break;
	default:
		return -EINVAL;
	}
	if(newpos < 0 || newpos > TMP_SIZE){
		return -EINVAL;
	}
	filp->f_pos = newpos;
	printk("mytmp lseek! filp->f_pos = %llu\n", filp->f_pos);
	return newpos;
}

static ssize_t read_tmp(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	unsigned long p = *ppos;
	unsigned int count = size;
	int ret = 0;
	if(p >= TMP_SIZE){
		return 0;
	}
	if(count > TMP_SIZE - p)
		count = TMP_SIZE - p;
	if(copy_to_user(buf, (void *)(dev->tmp + p), count)){
		ret = -EFAULT;
	}
	else{
		*ppos += count;
		ret = count;
		printk("read %d bytes from %ld\n", count, p);
	}
	return ret;
}	

static ssize_t write_tmp(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	unsigned long p = *ppos;
	unsigned int count = size;
	int ret = 0;
	if(p >= TMP_SIZE)
		return count?-ENXIO:0;
	if(count > TMP_SIZE - p)
		count = TMP_SIZE - p;
	if((copy_from_user(dev->tmp + p, buf, count)))
		ret = -EFAULT;
	else{
		*ppos += count;
		ret = count;
		printk("write %d bytes from %ld\n", count, p);
	}
	return ret;
}

static struct file_operations mytmp_fops = {
	.owner = THIS_MODULE,
	.open = open_tmp,
	.release = release_tmp,
	.llseek = lseek_tmp,
	.read = read_tmp,
	.write = write_tmp,
};

static void mytmp_setup_cdev(struct mytmp_cdev *dev, int index)
{
	int err, devno = MKDEV(mytmp_major, index);
	cdev_init(&dev->cdev, &mytmp_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &mytmp_fops;
	err = cdev_add(&dev->cdev, devno, 1);
	if(err)
		printk("error %d adding mytmp %d\n", err, index);
}

static int __init mytmp_init(void)
{
	int result;
	dev_t devno = MKDEV(mytmp_major, 0);
	if(mytmp_major){
		result = register_chrdev_region(devno, 1, CDEV_NAME);
	}
	else{
		result = alloc_chrdev_region(&devno, 0, 1, CDEV_NAME);
		mytmp_major = MAJOR(devno);
	}
	if(result < 0){
		printk("Can't get major devno:%d \n",mytmp_major);
		return result;
	}
	dev = (mytmp_cdev *)kmalloc(sizeof(struct mytmp_cdev), GFP_KERNEL);
	if(!dev){
		result = -ENOMEM;
		goto fail_malloc;
	}
	memset(dev, 0, sizeof(struct mytmp_cdev));
	mytmp_setup_cdev(dev, 0);
	return 0;
fail_malloc:
	unregister_chrdev_region(devno, 1);
	return result;
}

static void __exit mytmp_exit(void)
{
	cdev_del(&dev->cdev);
	kfree(dev);
	unregister_chrdev_region(MKDEV(mytmp_major, 0), 1);
	printk("mytmp exit!\n");
}

module_init(mytmp_init);
module_exit(mytmp_exit);
