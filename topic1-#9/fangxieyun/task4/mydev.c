#include "mydev.h"

static int __init mydev_init(void)
{
	mydev_null_init();
	mydev_zero_init();
	mydev_temp_init();
	return 0;
}

static void __exit mydev_exit(void)
{
	mydev_null_exit();
	mydev_zero_exit();
	mydev_temp_exit();
}

MODULE_AUTHOR("Fang Xieyun");
MODULE_LICENSE("GPL");

module_init(mydev_init);
module_exit(mydev_exit);
