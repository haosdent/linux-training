#include <sys/types.h> 
#include <sys/stat.h> 
#include <string.h> 
#include <stdio.h> 
#include <fcntl.h> 

int main(void) 
{ 
  int fd,i; 
  char buf[4096]; 
  char get[4096]; 
  memset(get, 0, sizeof(get)); 
  memset(buf, 0, sizeof(buf)); 
  fd = open("/dev/ch/mydev_temp", O_RDWR, S_IRUSR|S_IWUSR);
  
  if (fd > 0) { 
	
    if(read(fd, buf, sizeof(buf)) < 0){ 
		printf("read end of File\n");
	}else{
		printf("The message in mydev_temp now is: %s\n", buf); 
	}

	printf("please enter a string you want input to mydriver:\n"); 
	scanf("%s",get); 
	lseek(fd,0,SEEK_SET);
    if(write(fd, get, sizeof(get))){
		printf("write success\n");
	}else{
		printf("write error\n");
	}	
	lseek(fd,0,SEEK_SET);
    if(read(fd, buf, sizeof(buf)) < 0){ 
		printf("read end of File\n");
	}else{
		printf("The message changed to: %s\n", buf);  
	}
	lseek(fd,0,SEEK_SET);
	
  }else { 
    printf("Open device error!\n"); 
    return -1; 
  } 
  close(fd);
  return 0; 
} 