##简介

这个文档适合在使用git前浏览一遍，以便在对git产生一个完整的理解。另外，这个文档不是完整的git使用手册，而是根据git用法的使用频度和为了方便理解的目的进行了摘要。

Git是分布式的版本控制软件。Git的命令比较多，我到目前也只用到了一些常用的命令。这里把我最常用的功能总结一下。

Git windows版本在这里下载安装：

	http://code.google.com/p/msysgit/downloads/

#克隆与跟踪项目代码

##git clone

git clone命令用于克隆项目代码，但是只会克隆master分支：

    git clone git@bitbucket.org:janzhou/linux-training.git

##git remote

这时候使用remote命令查看远程池，会有一个origin：

    $ git remote -v
    origin  git@bitbucket.org:janzhou/linux-training.git (fetch)
    origin  git@bitbucket.org:janzhou/linux-training.git (push)

跟踪项目上的其它分支，则需要使用checkout命令：

##git branch

git branch用于查看本地分支：

    $ git branch
    * master

##git checkout

    $ git checkout -t origin/janzhou
    Branch janzhou set up to track remote branch janzhou from origin.
    Switched to a new branch 'janzhou'

运行之后，则切换到janzhou分支。此时，查看分支:

    $ git branch
    * janzhou
      master

多了一个janzhou分支，前面的*号表示当前所在的分支。

##Commit对象

Git的基本对象是commit，中文理解为一次提交。提交之前，先用git add命令加入所要提交的文件，使用参数-A则会加入所有的修改。提交方法为：

    $ git add -A
	$ git commit -m "描述信息"

每次提交都产生一个以hash命名的对象。可用git log查看记录：

	$ git log
	commit b0e1a48762245673d7848ac4a27a0f2b90f0335d
	Author: Jian Zhou <zhoujian@tiaohanshu.com>
	Date:   Sun Dec 9 13:34:48 2012 +0800
	
	    描述信息

可以通过checkout命令提取某次提交：

	git checkout <hash>

<hash>可以取hash值的前面任意位，只要是唯一值，不产生冲突。


##分支

一个连续的提交链组成一个分支。Git可以任意的建立和删除分支。建立分支有多种命令，其中最基本的命令如下：

	git branch <branchname>

比如，建立一个test分支

	git branch test

查看分支：

	$ git branch
	* master
	  test

master前面有＊号，说明当前在master分支下。master是一个特殊的分支，是git创建的时候就存在的，不可删除的分支。 跳转到某个分支上用checkout命令：

	git checkout <branchname>

git branch和checkout命令可合并执行：

    git checkout -b <branchname>

此命令分新建并切换到新建分支上。

##远程与本地

需要注意到commit, branch和checkout命令都是操作在本地，不会影响到远程池。git允许有多个远程池。查看远程的方法为：

    $ git remote -v
    origin  git@bitbucket.org:janzhou/linux-training.git (fetch)
    origin  git@bitbucket.org:janzhou/linux-training.git (push)

管理远程的命令全在git remote下，可以通过git remote -h查看用法。注意这里origin和前面提到的master类似，是git的默认远程池。

查看远程分支：

	git ls-remote [远程]

例如，查看origin下的分支：

    $ git ls-remote
    From git@bitbucket.org:janzhou/linux-training.git
    ca54622c0ca326ba04df6d9db8dc3cfe668a914f        HEAD
    dcd3c609e2bea8c22dc7ddcc63bfaf7fdfe1b915        refs/heads/ailei
    656db672c0c5dfa4ec3e4a55fe58c6c16ba658fe        refs/heads/chenpeng
    d83954692df0b4d78663a031fdd00cb1c7091291        refs/heads/fangxieyun
    bb1fbcd99c2ee7ed942d3661a61dc5892505b200        refs/heads/haitao
    ca54622c0ca326ba04df6d9db8dc3cfe668a914f        refs/heads/janzhou
    ca54622c0ca326ba04df6d9db8dc3cfe668a914f        refs/heads/master
    6dde7a0faa60b856feaf46df23581c37aeb9e1ec        refs/heads/zhouyou
    87a7ed82311afaf1110fff29c0accc74d97f013a        refs/tags/topic1
    8c8737cfc47b9f6ea5b4d41095a7b121cd76cdc8        refs/tags/topic2

##push与pull

git可以与远程打交道的是push和pull命令。push用于向远程提交：

	git push [远程] ［本地分支］：［远程分支］

这个命令有一些简短用法：

	git push //向origin提交当前分支
	git push [远程] //向远程提交当前分支
	git push [远程] :[远程分支] //删除远程上的分支

git push会检查本地分支与远程分支是否一致。如果发现不一至会报错。比如出现这种情况：在得到远程后你做了修改，然后push，但是在你push之前，其它人也push过修改，你的push会影响到别人的提交，这时就产生分支的冲突。这时就需要先从远程上pull到这个修改，在本地消除这种冲突后再上传，这样就能够保留别人做的修改。pull和push有类似的用法：

	git push [远程] ［本地分支］：［远程分支］

pull的简短用法参照push。冲突的处理请看分支合并一节。

##分支合并

在新的分支下修改，修改完成后再合并到master分支下是好的选择，这样可以防止失误操作丢失数据。合并的命令是：

	git merge [所选分支]

该命令把所选分支合并到当前分支下。很多修改都能自动合并，但是会有一些不能自动处理的修改。如果遇到，git会有提示，这时就需要手动来处理这些冲突(哪些文件有冲突会提示，打开文件，冲突的地方要编辑，编辑完后commit，然后push)。

pull命令也有merge类似的合并操作，不同的是合并的对象是远程分支。

##总结

提交修改：

	git add -A
	git commit -m "描述"
	git push

与远程同步：

	git pull
