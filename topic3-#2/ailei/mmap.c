#include <linux/module.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>

//#include <linux/>

MODULE_LICENSE("GPL");

static int my_major = 0;
module_param(my_major, int, S_IRUGO);

#ifndef CDEV_SIZE
#define CDEV_SIZE 4096
#endif

struct my_cdev
{
	char *data;
	unsigned long size;
};
struct my_cdev *cdevp;
struct cdev mycdev;

int mycdev_open(struct inode *inode, struct file *filp)
{
	struct my_cdev *dev;
	
	dev = cdevp;
	filp->private_data = dev;
	
	return 0;
}

int mycdev_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static int mydev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	struct my_cdev *dev = filp->private_data;
	vma->vm_flags |= VM_IO;
	vma->vm_flags |= VM_RESERVED;
	
	if(remap_pfn_range(vma,vma->vm_start,virt_to_phys(dev->data)>>PAGE_SHIFT, vma->vm_end - vma->vm_start, vma->vm_page_prot))
		return -EAGAIN;
		
	return 0;	
}

static const struct file_operations my_fops =
{
	.owner = THIS_MODULE,
	.open = mycdev_open,
	.release = mycdev_release,
	.mmap = mydev_mmap,
};

static int mycdev_init(void)
{
	int result;
	
	dev_t devno = MKDEV(my_major, 0);
	if (my_major)
		result = register_chrdev_region(devno, 1, "mycdev");
	else
	{
		result = alloc_chrdev_region(&devno, 0, 1, "mycdev");
		my_major = MAJOR(devno);
	}
	
	cdev_init(&mycdev, &my_fops);
	mycdev.owner = THIS_MODULE;
	mycdev.ops = &my_fops;
	
	cdev_add(&mycdev, MKDEV(my_major, 0), 1);
	
	cdevp = kmalloc(sizeof(struct my_cdev), GFP_KERNEL);
	if (!cdevp)
	{
		result = -ENOMEM;
		goto fail_malloc;
	}
	memset(cdevp, 0, sizeof(struct my_cdev));
	
	cdevp[0].size = CDEV_SIZE;
	cdevp[0].data = kmalloc(CDEV_SIZE, GFP_KERNEL);
	memset(cdevp[0].data, 0, CDEV_SIZE);
	
	return 0;
	
	fail_malloc:
		unregister_chrdev_region(devno, 1);
	//printk("cdev_init\n");
	return result;
}

static void mycdev_exit(void)
{
	//printk("cdev_exit\n");
	cdev_del(&mycdev);
	kfree(cdevp);
	unregister_chrdev_region(MKDEV(my_major, 0), 1);
}

module_init(mycdev_init);
module_exit(mycdev_exit);