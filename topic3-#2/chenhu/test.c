#include<sys/mman.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>

int main(){
    int fd;
    char *start,*buf;
   if((fd=open("/dev/my_mmap",O_RDWR))<0)
       printf("open error!\n");
   buf=(char *)malloc(150);
   memset(buf,0,150);
   start=mmap(NULL,150,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
   strcpy(start,"hello,world!");
   strcpy(buf,start);
   printf("buf=%s\n",buf);
   munmap(start,150);
   free(buf);
   close(fd);
   return 0;
}




