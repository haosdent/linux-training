//用户空间中，应该如何获得共享内存区域的地址。
//mknod  /dev/memcdev c  249  0

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>

int main()
{
	int fd;
	char *mem_start;
	char s[]="hello world,mmap.\n";
	char s_read[4096];
	
	fd = open("/dev/memcdev", O_RDWR);
	
	if( ( mem_start = mmap( NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0))
       	  == MAP_FAILED)
	{
		printf("mmap failed.\n");
		exit(0);
	}
	
	write(fd,s,sizeof(s));
	read(fd,s_read,sizeof(s));
	printf("mem:%s\n", s_read);
	
	return 0;
}