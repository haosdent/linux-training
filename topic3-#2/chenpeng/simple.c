#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>   /* printk() */
#include <linux/slab.h>   /* kmalloc() */
#include <linux/fs.h>       /* everything... */
#include <linux/errno.h>    /* error codes */
#include <linux/types.h>    /* size_t */
#include <linux/mm.h>
#include <linux/kdev_t.h>
#include <asm/page.h>
#include <linux/cdev.h>
#include <linux/vmalloc.h>  /* vmalloc() */
#include <linux/device.h>

static int simple_major = 0;
module_param(simple_major, int, 0);

MODULE_AUTHOR("Jonathan Corbet");
MODULE_LICENSE("Dual BSD/GPL");

#define SHARE_MEM_PAGE_COUNT 2
#define SHARE_MEM_SIZE (PAGE_SIZE * SHARE_MEM_PAGE_COUNT)

char *share_mem = NULL;

static int simple_open (struct inode *inode, struct file *filp)
{
	return 0;
}

static int simple_release(struct inode *inode, struct file *filp)
{
	return 0;
}

void simple_vma_open(struct vm_area_struct *vma)
{
	printk(KERN_NOTICE "Simple VMA open, virt %lx, phys %lx\n\n",
			vma->vm_start, vma->vm_pgoff << PAGE_SHIFT);
}

void simple_vma_close(struct vm_area_struct *vma)
{
	printk(KERN_NOTICE "Simple VMA close.\n");
}


/*
 * The remap_pfn_range version of mmap
 */

static struct vm_operations_struct simple_remap_vm_ops = {
	.open =  simple_vma_open,
	.close = simple_vma_close,
};

static int simple_remap_mmap(struct file *filp, struct vm_area_struct *vma)
{
	printk("\n================Call remap_pfn_range===============\n");
    printk("%-25s %08lx\n", "1)vma->vm_start", vma->vm_start);
    printk("%-25s %08lx\n", "2)vma->vm_end", vma->vm_end);
    printk("%-25s %08lx\n", "3)vma->vm_pgoff", vma->vm_pgoff);
	printk("%-25s %08lx\n", "4)vma->vm_page_prot", vma->vm_page_prot);
	printk("=====================================================\n");
	//调用remap_pfn_range函数建立页表
	if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
			    vma->vm_end - vma->vm_start,
			    vma->vm_page_prot))
		return -EAGAIN;
	
	vma->vm_ops = &simple_remap_vm_ops;
	simple_vma_open(vma);
	return 0;
}

/************************************************************
当访问的页面不在内存，即发生缺页中断时，nopage就会被调用。
这是因为，当发生缺页中断时，系统会经过如下处理过程：
1.找到缺页的虚拟地址所在的VMA。
2.如果必要，分配中间页目录表和页表。
3.如果页表项对应的物理页面不存在，则调用nopage函数，它返回物理页面的页描述符。
4.将物理页面的地址填充到页表中。
**********************************************************************/

/*
 * The nopage version.
 */

static int simple_vma_fault(struct vm_area_struct *vma, struct vm_fault *vmf)
{
	struct page *page;
	unsigned long offset;
	void *page_ptr;
	
	printk("\n==============Call simple_vma_fault===========\n");
    printk("%-25s %08lx\n", "1)vmf->flags", vmf->flags);
    printk("%-25s %08lx\n", "2)vmf->pgoff", vmf->pgoff);
    printk("%-25s %08lx\n", "3)vmf->virtual_address", vmf->virtual_address);
    printk("%-25s %08lx\n", "4)vma->vm_start", vma->vm_start);
    printk("%-25s %08lx\n", "5)vma->vm_end", vma->vm_end);
    printk("%-25s %08lx\n", "6)vma->vm_pgoff", vma->vm_pgoff);
	printk("===============================================\n");
	page_ptr = NULL;
	
	if((NULL == vma) || (NULL == share_mem)){
		printk("return VM_FAULT_SIGBUS!\n");
		return VM_FAULT_SIGBUS;
	}
	
	offset = (unsigned long)vmf->virtual_address - vma->vm_start;
	if(offset >= SHARE_MEM_SIZE){
		printk("return VM_FAULT_SIGBUS!");
        return VM_FAULT_SIGBUS;
	}
	
	page_ptr = share_mem + offset;
	page = vmalloc_to_page(page_ptr);//virt_to_page
	get_page(page);
	vmf->page = page;

	return 0;
}

static struct vm_operations_struct simple_nopage_vm_ops = {
	.open =   simple_vma_open,
	.close =  simple_vma_close,
	//.nopage = simple_vma_nopage,
	.fault = simple_vma_fault,
};

static int simple_nopage_mmap(struct file *filp, struct vm_area_struct *vma)
{
	unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;

	if (offset >= __pa(high_memory) || (filp->f_flags & O_SYNC))
		vma->vm_flags |= VM_IO;
	vma->vm_flags |= VM_RESERVED;

	vma->vm_ops = &simple_nopage_vm_ops;
	simple_vma_open(vma);
	printk("============================================\n");
	printk("==========call simple_nopage_mmap===========\n");
	printk("============================================\n");
	return 0;
}


/*
 * Set up the cdev structure for a device.
 */
static void simple_setup_cdev(struct cdev *dev, int minor,
		struct file_operations *fops)
{
	int err, devno = MKDEV(simple_major, minor);
    
	cdev_init(dev, fops);
	dev->owner = THIS_MODULE;
	dev->ops = fops;
	err = cdev_add (dev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk (KERN_NOTICE "Error %d adding simple%d", err, minor);
}

/*************************************************
当用户调用mmap系统调用时,内核会进行如下处理:
1.在进程的虚拟空间查找一块VMA.
2.将这块VMA进行映射.
3.如果设备驱动程序中定义了mmap函数,则调用它.
4.将这个VMA插入到进程的VMA链表中.
内存映射工作大部分由内核完成，驱动程序中的mmap函数只需要为该地址范围建立合适的页表，
并将vma->vm_ops替换为一系列的新操作就可以了。
有两种建立页表的方法，一是使用remap_pfn_range函数一次全部建立，
或者通过nopage方法每次建立一个页表。
**************************************************/

/*
 * Our various sub-devices.
 */
/* Device 0 uses remap_pfn_range */
static struct file_operations simple_remap_ops = {
	.owner   = THIS_MODULE,
	.open    = simple_open,
	.release = simple_release,
	.mmap    = simple_remap_mmap,
};

/* Device 1 uses nopage */
static struct file_operations simple_nopage_ops = {
	.owner   = THIS_MODULE,
	.open    = simple_open,
	.release = simple_release,
	.mmap    = simple_nopage_mmap,
};

#define MAX_SIMPLE_DEV 2

#if 0
static struct file_operations *simple_fops[MAX_SIMPLE_DEV] = {
	&simple_remap_ops,
	&simple_nopage_ops,
};
#endif

/*
 * We export two simple devices.  There's no need for us to maintain any
 * special housekeeping info, so we just deal with raw cdevs.
 */
static struct cdev SimpleDevs[MAX_SIMPLE_DEV];

/*
 * Module housekeeping.
 */
static int simple_init(void)
{
	int result, i;
	dev_t dev = MKDEV(simple_major, 0);

	/* Figure out our device number. */
	if (simple_major)
		result = register_chrdev_region(dev, 2, "simple");
	else {
		result = alloc_chrdev_region(&dev, 0, 2, "simple");
		simple_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "simple: unable to get major %d\n", simple_major);
		return result;
	}
	if (simple_major == 0)
		simple_major = result;
		
	share_mem = (char *)vmalloc(SHARE_MEM_SIZE);
	printk("=======printk share_mem=========\n");
	printk("share_mem = %x\n", share_mem);
	printk("================================\n");
	for (i = 0; i < SHARE_MEM_PAGE_COUNT; i++)
		sprintf(share_mem + PAGE_SIZE * i, "Test %d", i);

	/* Now set up two cdevs. */
	simple_setup_cdev(SimpleDevs, 0, &simple_remap_ops);
	simple_setup_cdev(SimpleDevs + 1, 1, &simple_nopage_ops);
	return 0;
}


static void simple_cleanup(void)
{
	if(share_mem != NULL)
		vfree(share_mem);
	cdev_del(SimpleDevs);
	cdev_del(SimpleDevs + 1);
	unregister_chrdev_region(MKDEV(simple_major, 0), 2);
}

module_init(simple_init);
module_exit(simple_cleanup);
