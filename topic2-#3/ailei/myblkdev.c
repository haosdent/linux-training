#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/spinlock.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/hdreg.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/slab.h>

#define BLK_MAJOR 250
#define BLK_NAME "myblkdev"
#define DISK_SECTOR_SIZE 512
#define DISK_BYTES (1024*1024*64)

int blk_major=0;

struct myblk_dev{
	int size;						/*以字节为单位，设备大小*/
	unsigned char *data;			/*数据数组*/
	short users;					/*用户数目*/
	struct request_queue *queue;	/*设备请求队列*/
	struct gendisk *gd;				/*gendisk结构*/
	spinlock_t lock;				/*用于互斥*/
};

struct myblk_dev *myblkdev;

/*块设备请求处理函数*/
static int myblk_make_request(struct request_queue *q,struct bio *bio)
{
	int i;
	char *mem_pbuf;
	char *disk_pbuf;
	struct myblk_dev *pmyblkdev;
	struct bio_vec *pbvec;
	if((bio->bi_sector*DISK_SECTOR_SIZE + bio->bi_size) > DISK_BYTES){
		bio_io_error(bio);
		return 0;
	}
	pmyblkdev = (struct myblk_dev*)bio->bi_bdev->bd_disk->private_data; /*得到设备结构体*/
	//printk("myblk_make_request\n");
	disk_pbuf = pmyblkdev->data+bio->bi_sector*DISK_SECTOR_SIZE;	/*得到要读写的起始位置*/
	
	/*开始遍历这个bio中的每个bio_vec*/
	bio_for_each_segment(pbvec,bio,i){	/*循环分散的内存segment*/
		mem_pbuf = kmap(pbvec->bv_page)+pbvec->bv_offset;/*获得实际内存地址*/
		switch(bio_data_dir(bio)){	/*读写*/
			case READA:
			case READ:
				memcpy(mem_pbuf, disk_pbuf, pbvec->bv_len);
				break;
			case WRITE:
				memcpy(disk_pbuf, mem_pbuf, pbvec->bv_len);
				break;
			default:
				kunmap(pbvec->bv_page);
				bio_io_error(bio);
				return 0;
		}
		kunmap(pbvec->bv_page);/*清除映射*/
		disk_pbuf += pbvec->bv_len;
	}
	bio_endio(bio,0);
	return 0;
}

static int blk_ioctl(struct block_device *dev, fmode_t no, unsigned cmd, unsigned long arg)
{
	printk("#############\n");
	return -ENOTTY;
}

static int blk_open(struct block_device *dev, fmode_t no)
{
	struct myblk_dev *device = dev->bd_inode->i_bdev->bd_disk->private_data;
	spin_lock(&device->lock);
	device->users ++;
	spin_unlock(&device->lock);
	
	printk("blk mount succeed\n");
	return 0;
}

static int blk_release(struct gendisk *gd, fmode_t no)
{
	struct myblk_dev *device = gd->private_data;
	spin_lock(&device->lock);
	device->users --;
	spin_unlock(&device->lock);
	
	printk("blk umount succeed\n");
	return 0;
}

struct block_device_operations blk_ops={
	.owner = THIS_MODULE,
	.open = blk_open,
	.release = blk_release,
	.ioctl = blk_ioctl,
};

static int __init myblkdev_init(void)
{
	blk_major = register_blkdev(blk_major,BLK_NAME);	/*注册驱动*/
	if(blk_major < 0){
		printk("unable to get major number\n");
		return -EBUSY;
	}else if(blk_major == 0){
		blk_major = BLK_MAJOR;
		printk("regiser blk dev succeed\n");
	}
	
	myblkdev = kmalloc (sizeof(struct myblk_dev), GFP_KERNEL);
	if(myblkdev == NULL){
		unregister_blkdev(blk_major,BLK_NAME);
		return -ENOMEM;
	}
	
	memset(myblkdev, 0, sizeof(struct myblk_dev));
	myblkdev->size = DISK_BYTES;			/*设备的大小*/
	myblkdev->data = vmalloc(DISK_BYTES);	/*设备的数据空间*/
	if(myblkdev->data == NULL){
		printk(KERN_NOTICE "vmalloc failure.\n");
		return -EBUSY;
	}
	spin_lock_init(&myblkdev->lock);
	
	myblkdev->queue = blk_alloc_queue(GFP_KERNEL);/*生成队列*/
	if(myblkdev->queue == NULL){
		printk("blk_alloc_queue failure\n");
		goto out_vfree;
	}
	blk_queue_make_request(myblkdev->queue,myblk_make_request);/*注册make_request，绑定请求制造函数*/
	blk_queue_logical_block_size(myblkdev->queue, DISK_SECTOR_SIZE);
	myblkdev->queue->queuedata = myblkdev;
	
	myblkdev->gd = alloc_disk(1);	/*生成gendisk*/
	if(! myblkdev->gd){
		printk("alloc_disk failure\n");
		goto out_vfree;
	}
	myblkdev->gd->major = blk_major;/*主设备号*/
	myblkdev->gd->first_minor = 0;/*次设备号*/
	myblkdev->gd->fops = & blk_ops;/*块文件结构变量*/
	myblkdev->gd->queue = myblkdev->queue;/*请求队列*/
	myblkdev->gd->private_data = myblkdev;/*私有数据指针*/
	snprintf(myblkdev->gd->disk_name,32,"myblkdev%c",'a');/*名字*/
	set_capacity(myblkdev->gd,DISK_BYTES>>9);/*块大小*/
	add_disk(myblkdev->gd);/*注册块设备*/
	
	printk("gendisk init success\n");
	return 0;
	
out_vfree:
	if(myblkdev->data){
		vfree(myblkdev->data);
	}
	return 0;
	
}

static void __exit myblkdev_exit(void)
{
	del_gendisk(myblkdev->gd);
	put_disk(myblkdev->gd);
	if(myblkdev->queue){
		kobject_put(&myblkdev->queue->kobj);
	}
	if(myblkdev->data){
		vfree(myblkdev->data);
	}
	unregister_blkdev(blk_major,BLK_NAME);
	kfree(myblkdev);
	printk("blk dev exit success!\n");
}

module_init(myblkdev_init);
module_exit(myblkdev_exit);

MODULE_AUTHOR("Fang Xieyun");
MODULE_LICENSE("GPL");