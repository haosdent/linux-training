//insmod my.ko
//module(init)

[10724.393659] begin of bdev_init     
                       
[10724.393664] bdev_init: register
[10724.393666] bdev_init: Devices = kmalloc
[10724.393668] bdev_init: setup_device

//setup_device第一次执行
[10724.393669] begin of setup_device
[10724.393671] setup_device: memset
[10724.393673] setup_device: dev->data = vmalloc
[10724.393739] setup_device: choose the request_mode
[10724.393801] setup_device: dev->gd = alloc_disk
[10724.393816] setup_device:add_disk    
 //open的入口在add_disk中。              
 //=================add_disk开始========================
[10724.394502] begin of open                           //open begin    
[10724.394512] begin of media_changed
[10724.394514] end of media_changed
[10724.394515] end of open                             //open end

[10724.394539] begin of revalidate
[10724.394541] end of revalidate

//一次请求(请求队列1)
[10724.394754] mode 0:begin of bdev_request            
[10724.394761] mode 0:bdev_transfer some sectors     
[10724.394762] begin of bdev_transfer                
[10724.394806] read 4096 bytes                       
[10724.394808] end of bdev_transfer                 
[10724.394819] mode 0:end of bdev_request  
//请求队列1中只有以上一个请求         
[10724.394821] mode 0:begin of bdev_request                          
[10724.394822] mode 0:end of bdev_request  
//一次请求(请求队列2)   
[10724.394861] mode 0:begin of bdev_request                 
[10724.394864] mode 0:bdev_transfer some sectors
[10724.394866] begin of bdev_transfer
[10724.394876] read 4096 bytes
[10724.394878] end of bdev_transfer
[10724.394882] mode 0:end of bdev_transfer
//请求队列2中只有以上一个请求
[10724.394884] mode 0:begin of bdev_request
[10724.394885] mode 0:end of bdev_request

//分区1:my_bdeva  (setup_device中)
[10724.394898]  my_bdeva: unknown partition table
[10724.394922] begin of release
[10724.394923] end of release
//=================add_disk结束========================

[10724.394925] ============================================================end of setup_device

//setup_device第二次执行
[10724.393669] begin of setup_device
[10724.393671] setup_device: memset
[10724.393673] setup_device: dev->data = vmalloc
[10724.393739] setup_device: choose the request_mode
[10724.393801] setup_device: dev->gd = alloc_disk
[10724.393816] setup_device:add_disk    
 //open的入口在add_disk中。              
 //=================add_disk开始========================
[10724.394502] begin of open                           //open begin    
[10724.394512] begin of media_changed
[10724.394514] end of media_changed
[10724.394515] end of open                             //open end

[10724.394539] begin of revalidate
[10724.394541] end of revalidate

//一次请求(请求队列1)
[10724.394754] mode 0:begin of bdev_request            
[10724.394761] mode 0:bdev_transfer some sectors     
[10724.394762] begin of bdev_transfer                
[10724.394806] read 4096 bytes                       
[10724.394808] end of bdev_transfer                 
[10724.394819] mode 0:end of bdev_request  
//请求队列1中只有以上一个请求         
[10724.394821] mode 0:begin of bdev_request                          
[10724.394822] mode 0:end of bdev_request  
//一次请求(请求队列2)   
[10724.394861] mode 0:begin of bdev_request                 
[10724.394864] mode 0:bdev_transfer some sectors
[10724.394866] begin of bdev_transfer
[10724.394876] read 4096 bytes
[10724.394878] end of bdev_transfer
[10724.394882] mode 0:end of bdev_transfer
//请求队列2中只有以上一个请求
[10724.394884] mode 0:begin of bdev_request
[10724.394885] mode 0:end of bdev_request

//分区1:my_bdeva  (setup_device中)
[10724.394898]  my_bdeva: unknown partition table
[10724.394922] begin of release
[10724.394923] end of release
//=================add_disk结束========================
[10724.422819] ============================================================end of setup_device

//setup_device第三次执行
[10724.393669] begin of setup_device
[10724.393671] setup_device: memset
[10724.393673] setup_device: dev->data = vmalloc
[10724.393739] setup_device: choose the request_mode
[10724.393801] setup_device: dev->gd = alloc_disk
[10724.393816] setup_device:add_disk    
 //open的入口在add_disk中。              
 //=================add_disk开始========================
[10724.394502] begin of open                           //open begin    
[10724.394512] begin of media_changed
[10724.394514] end of media_changed
[10724.394515] end of open                             //open end

[10724.394539] begin of revalidate
[10724.394541] end of revalidate

//一次请求(请求队列1)
[10724.394754] mode 0:begin of bdev_request            
[10724.394761] mode 0:bdev_transfer some sectors     
[10724.394762] begin of bdev_transfer                
[10724.394806] read 4096 bytes                       
[10724.394808] end of bdev_transfer                 
[10724.394819] mode 0:end of bdev_request  
//请求队列1中只有以上一个请求         
[10724.394821] mode 0:begin of bdev_request                          
[10724.394822] mode 0:end of bdev_request  
//一次请求(请求队列2)   
[10724.394861] mode 0:begin of bdev_request                 
[10724.394864] mode 0:bdev_transfer some sectors
[10724.394866] begin of bdev_transfer
[10724.394876] read 4096 bytes
[10724.394878] end of bdev_transfer
[10724.394882] mode 0:end of bdev_transfer
//请求队列2中只有以上一个请求
[10724.394884] mode 0:begin of bdev_request
[10724.394885] mode 0:end of bdev_request

//分区1:my_bdeva  (setup_device中)
[10724.394898]  my_bdeva: unknown partition table
[10724.394922] begin of release
[10724.394923] end of release
//=================add_disk结束========================
[10724.423767] ============================================================end of setup_device

//setup_device第四次执行
[10724.393669] begin of setup_device
[10724.393671] setup_device: memset
[10724.393673] setup_device: dev->data = vmalloc
[10724.393739] setup_device: choose the request_mode
[10724.393801] setup_device: dev->gd = alloc_disk
[10724.393816] setup_device:add_disk    
 //open的入口在add_disk中。              
 //=================add_disk开始========================
[10724.394502] begin of open                           //open begin    
[10724.394512] begin of media_changed
[10724.394514] end of media_changed
[10724.394515] end of open                             //open end

[10724.394539] begin of revalidate
[10724.394541] end of revalidate

//一次请求(请求队列1)
[10724.394754] mode 0:begin of bdev_request            
[10724.394761] mode 0:bdev_transfer some sectors     
[10724.394762] begin of bdev_transfer                
[10724.394806] read 4096 bytes                       
[10724.394808] end of bdev_transfer                 
[10724.394819] mode 0:end of bdev_request  
//请求队列1中只有以上一个请求         
[10724.394821] mode 0:begin of bdev_request                          
[10724.394822] mode 0:end of bdev_request  
//一次请求(请求队列2)   
[10724.394861] mode 0:begin of bdev_request                 
[10724.394864] mode 0:bdev_transfer some sectors
[10724.394866] begin of bdev_transfer
[10724.394876] read 4096 bytes
[10724.394878] end of bdev_transfer
[10724.394882] mode 0:end of bdev_transfer
//请求队列2中只有以上一个请求
[10724.394884] mode 0:begin of bdev_request
[10724.394885] mode 0:end of bdev_request

//分区1:my_bdeva  (setup_device中)
[10724.394898]  my_bdeva: unknown partition table
[10724.394922] begin of release
[10724.394923] end of release
//=================add_disk结束========================
[10724.442181] ============================================================end of setup_device

[10724.446700] end of bdev_init
