#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/spinlock.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/hdreg.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/slab.h>

#define MAP_MAJOR 250
#define MAP_NAME "mymap"
#define DISK_SECTOR_SIZE 512
#define FILE_NAME "testfile"
#define TABLE_NAME "testtable"
#define TABLE_SIZE 4096
#define TEMP_PAGE_SIZE 4096
#define BLOCK_PAGE 512
#define TEMP_BLOCK_SIZE (BLOCK_PAGE*TEMP_PAGE_SIZE)


int map_major=0;

struct mymap_dev{
	int users;					/*用户数目*/
	struct file *filp;				/*数据文件指针*/
	struct file *tablefilp;			/*表文件指针*/
	unsigned long long size;		/*以字节为单位，设备大小，实际为文件大小*/
	struct request_queue *queue;	/*设备请求队列*/
	struct gendisk *gd;				/*gendisk结构*/
	struct mutex ctl_mutex;			/*用于互斥*/
	spinlock_t lock;				/*用于互斥*/
	wait_queue_head_t event;		/*启动事件*/
	struct task_struct *thread;		/*任务线程*/
	struct bio_list mybio_list;
};

struct lpn_ppn{
	long long lpn;
	long long ppn;
};

struct table_index{
	int start;
	int end;
};

struct lbn_pbn{
	long long lbn;
	long long pbn;
};

struct mymap_dev *mymapdev;
struct lpn_ppn *table;
struct table_index table_id;
struct lbn_pbn *memory_table;
unsigned long long memory_table_size;

static long long file_lpn_ppn(struct file *file, long long lpn)
{
	long long ppn = -1;
	unsigned long long lbn = lpn/BLOCK_PAGE;
	unsigned long long pbn;
	unsigned char buf[128];
	struct lpn_ppn temp_lpn_ppn;
	loff_t pos;
	int i;
	sprintf(buf,"%8llu %8llu\n",table->lpn,table->ppn);
	for(i=0; i<BLOCK_PAGE; ++i){
		pbn = (memory_table+lbn)->pbn;
		pos = pbn*BLOCK_PAGE + lpn%BLOCK_PAGE;
		file->f_op->llseek(file,strlen(buf)*pos,0);
		file->f_op->read(file,(char *)buf,strlen(buf),&file->f_pos);
		sscanf(buf,"%llu %llu\n",&temp_lpn_ppn.lpn,&temp_lpn_ppn.ppn);
		if(temp_lpn_ppn.lpn == lpn){
			ppn = temp_lpn_ppn.ppn;
			/* if(ppn != lpn){
				printk("lpn=%llu\tppn=%llu\n",lpn,ppn);
				printk("@@@@@@@\n");
			} */
			break;
		}
	}
	return ppn;
}

static int insert_table(struct lpn_ppn lpnppn)
{
	if((table_id.end+1) % TABLE_SIZE == table_id.start){
		printk("table full\n");
		return -1;
	}
	table[table_id.end].lpn = lpnppn.lpn;
	table[table_id.end].ppn = lpnppn.ppn;
	table_id.end = (table_id.end + 1) % TABLE_SIZE;
	return 0;
}

static int delete_table(struct lpn_ppn *lpnppn)
{
	if(table_id.start == table_id.end){
		printk("table null\n");
		return -1;
	}
	lpnppn->lpn = table[table_id.start].lpn;
	lpnppn->ppn = table[table_id.start].ppn;
	//printk("delete_table:table_id.start=%d\n",table_id.start);
	//printk("table[table_id.start].lpn=%llu table[table_id.start].ppn=%llu\n",table[table_id.start].lpn, table[table_id.start].ppn);
	table_id.start = (table_id.start + 1) % TABLE_SIZE;
	return 0;
}

static long long table_lpn_ppn(struct file *file, long long lpn)
{
	int i;
	long long ppn = -1;
	struct lpn_ppn temp_lpnppn;
	if(table_id.start == table_id.end){
		printk("table_lpn_ppn:memory table null\n");
	}else{
		for(i=table_id.start; i != table_id.end; i=(i+1)%TABLE_SIZE){
			if((table+i)->lpn == lpn){
				ppn = (table+i)->ppn;
				break;
			}
		}
	}
	if(ppn < 0){
		ppn = file_lpn_ppn(file, lpn);
		if((table_id.end+1) % TABLE_SIZE == table_id.start){
			delete_table(&temp_lpnppn);
			//printk("temp_lpnppn.lpn=%llu temp_lpnppn.ppn=%llu\n",temp_lpnppn.lpn,temp_lpnppn.ppn);
		}
		temp_lpnppn.lpn = lpn;
		temp_lpnppn.ppn = ppn;
		insert_table(temp_lpnppn);
	}
	return ppn;
}

static int __do_write(struct file *file, u8 *buf, const int len, loff_t pos)
{
	ssize_t bw;
	mm_segment_t old_fs = get_fs();
	set_fs(get_ds());
	bw = file->f_op->write(file,buf,len,&pos);
	set_fs(old_fs);
	if(bw == len){
		return 0;
	}else{
		printk("Write erroe at byte offset %llu,lenght %i.\n",(unsigned long long)pos, len);
		return -EIO;
	}
}

static int do_write(struct mymap_dev *mymapdevp, struct bio *bio, loff_t pos)
{
	struct bio_vec *bvec;
	int i, ret = 0;
	
	bio_for_each_segment(bvec, bio, i){
		ret = __do_write(mymapdevp->filp,
			kmap(bvec->bv_page)+bvec->bv_offset,
			bvec->bv_len, pos);
		kunmap(bvec->bv_page);
		if(ret < 0)
			break;
		pos += bvec->bv_len;
	}
	
	return ret;
}

static int __do_read(struct file *file, u8 *buf, const int len, loff_t pos)
{
	ssize_t bw;
	mm_segment_t old_fs = get_fs();
	set_fs(get_ds());
	bw = file->f_op->read(file,buf,len,&pos);
	set_fs(old_fs);
	if(bw == len){
		return 0;
	}else{
		printk("Write erroe at byte offset %llu,lenght %i.\n",(unsigned long long)pos, len);
		return -EIO;
	}
}

static int do_read(struct mymap_dev *mymapdevp, struct bio *bio, loff_t pos)
{
	struct bio_vec *bvec;
	int i, ret = 0;

	bio_for_each_segment(bvec, bio, i){
		ret = __do_read(mymapdevp->filp,
			kmap(bvec->bv_page)+bvec->bv_offset,
			bvec->bv_len, pos);
		kunmap(bvec->bv_page);
		if(ret < 0)
			break;
		pos += bvec->bv_len;
	}
	return ret;
}

static int do_bio_file(struct mymap_dev *mymapdevp, struct bio *bio)
{
	int ret;
	loff_t pos;
	//loff_t pos = (loff_t)bio->bi_sector << 9;
	long long lpn = bio->bi_sector >> 3;
	long long offset = bio->bi_sector % 8;
	long long ppn = table_lpn_ppn(mymapdevp->tablefilp, lpn);
	//long long ppn = file_lpn_ppn(mymapdevp->tablefilp, lpn);
	if(lpn != ppn){
		printk("do_bio_file:lpn=%llu\tppn=%llu\n",lpn,ppn);
		printk("do_bio_file:##########\n");
	}
	pos = (loff_t)((ppn << 3) + offset) << 9;
	if(bio_rw(bio) == WRITE){
		ret = do_write(mymapdevp, bio, pos);
	}else{
		ret = do_read(mymapdevp, bio, pos);
	}
	return ret;
}

static void mymap_handle_bio(struct mymap_dev *mymapdevp, struct bio *bio)
{
	int ret = do_bio_file(mymapdevp, bio);
	bio_endio(bio,ret);
}

static int mymap_thread(void *data)
{
	struct mymap_dev *mymapdevp = (struct mymap_dev *)data;
	struct bio *bio;
	set_user_nice(current, -20);
	while(!kthread_should_stop() || !bio_list_empty(&mymapdevp->mybio_list)){
		wait_event_interruptible(mymapdev->event,
			!bio_list_empty(&mymapdev->mybio_list) || kthread_should_stop());
		if(bio_list_empty(&mymapdev->mybio_list))
			continue;
		spin_lock_irq(&mymapdevp->lock);
		bio = bio_list_pop(&mymapdevp->mybio_list);
		spin_unlock_irq(&mymapdevp->lock);
		mymap_handle_bio(mymapdevp, bio);
	}
	return 0;
}

/*块设备请求处理函数*/
static int mymap_make_request(struct request_queue *q,struct bio *bio)
{
	struct mymap_dev *mymapdevp = q->queuedata;
	spin_lock_irq(&mymapdevp->lock);
	bio_list_add(&mymapdevp->mybio_list, bio);
	wake_up(&mymapdevp->event);
	spin_unlock_irq(&mymapdevp->lock);
	return 0;
}

static int mymap_ioctl(struct block_device *dev, fmode_t no, unsigned cmd, unsigned long arg)
{
	printk("cmd=%u\n",cmd);
	printk("ioctl:#############\n");
	return -ENOTTY;
}

static int mymap_open(struct block_device *dev, fmode_t no)
{
	struct mymap_dev *mymapdevp = dev->bd_disk->private_data;
	
	mutex_lock(&mymapdevp->ctl_mutex);
	mymapdevp->users ++;
	mutex_unlock(&mymapdevp->ctl_mutex);
	
	printk("mymap_open:table_id.start=%d table_id.end=%d\n",table_id.start,table_id.end);
	printk("blk mount succeed\n");
	return 0;
}

static int mymap_release(struct gendisk *gd, fmode_t no)
{
	struct mymap_dev *mymapdevp = gd->private_data;
	
	mutex_lock(&mymapdevp->ctl_mutex);
	mymapdevp->users --;
	mutex_unlock(&mymapdevp->ctl_mutex);
	
	printk("mymap_en:table_id.start=%d table_id.end=%d\n",table_id.start,table_id.end);
	printk("blk umount succeed\n");
	return 0;
}

struct block_device_operations mymap_ops={
	.owner = THIS_MODULE,
	.open = mymap_open,
	.release = mymap_release,
	.ioctl = mymap_ioctl,
};

static int init_table(struct mymap_dev *mymapdevp)
{
	int i,j;
	struct lpn_ppn temp_lpn_ppn;
	struct file *filp;
	unsigned char buf[128];
	mm_segment_t old_fs = get_fs();
	unsigned long long filesize=mymapdevp->size;
	printk("init_table: filesize=%llu\n",filesize);
	filp = mymapdevp->tablefilp = filp_open(TABLE_NAME,O_RDWR,0);
	if(IS_ERR(mymapdev->tablefilp)){
		printk("error occured while opening file %s, exiting...\n",TABLE_NAME);
		return -1;
	}
	table = kzalloc (sizeof(struct lpn_ppn)*TABLE_SIZE, GFP_KERNEL);
	if(table == NULL){
		printk("table:kzalloc failure\n");
		return -1;
	}
	table_id.start = table_id.end = 0;
	
	memory_table_size = filesize/TEMP_BLOCK_SIZE;
	printk("memory_table_size=%llu\n",memory_table_size);
	
	memory_table = kzalloc(sizeof(struct lbn_pbn)*memory_table_size, GFP_KERNEL);
	if(memory_table == NULL){
		printk("memory_table:kzalloc failure\n");
		return -1;
	}
	set_fs(get_ds());
	for(i=0; i<memory_table_size; ++i){
		(memory_table+i)->lbn = (memory_table+i)->pbn = i;
		for(j=0; j<BLOCK_PAGE; ++j){
			temp_lpn_ppn.lpn = temp_lpn_ppn.ppn = i*BLOCK_PAGE + j;
			sprintf(buf,"%8llu %8llu\n",temp_lpn_ppn.lpn,temp_lpn_ppn.ppn);
			filp->f_op->llseek(filp,strlen(buf)*temp_lpn_ppn.ppn,0);
			filp->f_op->write(filp,(char *)buf,strlen(buf),&filp->f_pos);
		}
	}
	set_fs(old_fs);
	printk("init_table success\n");
	
	return 0;
}

static int __init mymap_init(void)
{
	unsigned long long filesize;
	map_major = register_blkdev(map_major,MAP_NAME);	/*注册驱动*/
	/* register_blkdev注册函数注册失败时，返回一个负值，如果主设备号参数为0，表示动态分配，分配成功返回主设备号，
		如果主设备号参数非0，表示静态分配，分配成功返回0*/
	if(map_major < 0){
		printk("unable to get major number\n");
		return -EBUSY;
	}else if(map_major == 0){
		map_major = MAP_MAJOR;
		printk("regiser blk dev succeed\n");
	}
	
	mymapdev = kzalloc (sizeof(struct mymap_dev), GFP_KERNEL);
	if(mymapdev == NULL){
		printk("kzalloc failure\n");
		goto out;
	}
	
	mymapdev->queue = blk_alloc_queue(GFP_KERNEL);/*生成队列*/
	if(mymapdev->queue == NULL){
		printk("blk_alloc_queue failure\n");
		goto out_free_dev;
	}
	
	bio_list_init(&mymapdev->mybio_list);
	
	blk_queue_make_request(mymapdev->queue,mymap_make_request);/*注册make_request，绑定请求制造函数*/
	// blk_queue_logical_block_size(mymapdev->queue, DISK_SECTOR_SIZE);/*告知内核块设备硬件扇区的大小*/
	mymapdev->queue->queuedata = mymapdev;/*queuedata是void *指针，用来指向所需要的数据*/
	
	
	mymapdev->thread = kthread_create(mymap_thread, mymapdev, "mymapdev%c", 'a');
	if(IS_ERR(mymapdev->thread)){
		mymapdev->thread = NULL;
		printk("kthread_create failure\n");
		goto out_free_queue;
	}
	wake_up_process(mymapdev->thread);
	
	/*分配gendisk，参数是这个磁盘使用的次设备号的数量，一般也就是磁盘分区的数量，此后minors不能被修改，数量1表示不包含分区*/
	mymapdev->gd = alloc_disk(1);	
	if(! mymapdev->gd){
		printk("alloc_disk failure\n");
		goto out_free_queue;
	}
	mutex_init(&mymapdev->ctl_mutex);
	init_waitqueue_head(&mymapdev->event);
	spin_lock_init(&mymapdev->lock);
	
	mymapdev->gd->major = map_major;/*主设备号*/
	mymapdev->gd->first_minor = 0;/*第一个设备号*/
	mymapdev->gd->fops = &mymap_ops;/*块文件结构变量*/
	
	mymapdev->gd->queue = mymapdev->queue;/*请求队列*/
	mymapdev->gd->private_data = mymapdev;/*私有数据指针*/
	snprintf(mymapdev->gd->disk_name,32,"mymapdev%c",'a');/*名字*/
	
	mymapdev->filp = filp_open(FILE_NAME,O_RDWR,0);
	if(IS_ERR(mymapdev->filp)){
		printk("error occured while opening file %s, exiting...\n",FILE_NAME);
		goto out_free_queue;
	}
	filesize = mymapdev->size = i_size_read(mymapdev->filp->f_mapping->host);/*获取文件大小，以字节为单位*/
	printk("mymapdev->size=%llu\n",mymapdev->size);
	set_capacity(mymapdev->gd,filesize>>9);/*设置gendisk容量*/
	
	if(init_table(mymapdev) < 0){
		printk("init_table error\n");
		goto out_free_queue;
	}
	
	/*增加gendisk,gendisk结构体被分配之后，系统还不能使用这个磁盘，需要调用如下函数来注册这个磁盘设备*/
	add_disk(mymapdev->gd);
	
	
	printk("gendisk init success\n");
	return 0;
	
out_free_queue:
	blk_cleanup_queue(mymapdev->queue);
out_free_dev:
	kfree(mymapdev);
out:
	unregister_blkdev(map_major,MAP_NAME);
	return 0;
	
}

static void __exit mymap_exit(void)
{
	filp_close(mymapdev->tablefilp, NULL);
	filp_close(mymapdev->filp, NULL);
	/*释放gendisk,当不再需要一个磁盘时，应当使用如下函数释放gendisk*/
	del_gendisk(mymapdev->gd);
	/*清除请求队列*/
	blk_cleanup_queue(mymapdev->queue);
	put_disk(mymapdev->gd);

	kfree(mymapdev);
	unregister_blkdev(map_major,MAP_NAME);
	printk("------------mymap exit success!-----------\n");
}


module_init(mymap_init);
module_exit(mymap_exit);

MODULE_AUTHOR("Fang Xieyun");
MODULE_LICENSE("GPL");