#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

MODULE_LICENSE("GPL");

int hello_init(void)
{	
	struct file *fp;
	mm_segment_t fs;
	loff_t pos;
	char buf[]="hello test1!\n";
	char buf1[20];
	//printk("hello init!\n");
	fp = filp_open("hello1", O_RDWR | O_CREAT, 0644);
	if (IS_ERR(fp))
	{
		printk("create file error!\n");
		return -1;
	}
	fs = get_fs();
	set_fs(KERNEL_DS);
	pos = 0;
	vfs_write(fp, buf, sizeof(buf), &pos);
	pos = 0;
	vfs_read(fp, buf1, sizeof(buf), &pos);
	printk("read:%s\n", buf1);
	filp_close(fp, NULL);
	set_fs(fs);
	return 0;
}

void hello_exit(void)
{
 	//printk("hello exit!\n");
}

module_init(hello_init);
module_exit(hello_exit);
