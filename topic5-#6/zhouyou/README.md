#blk_dev将块设备和文件映射起来

将文件模拟为SSD，按页写，不支持覆盖写。
使用dd命令向块设备发送具有（起始地址，块大小）的I/O请求，模拟文件系统向SSD下发请求。

##1. 关键的数据结构

	#define FILE_DIR "/tmp/blkdev_file"      //数据文件将设备数据映射到某个文件，定义数据文件的路径。
	#define MAP_TABLE "/tmp/map_file"        //映射表文件。行号表示lpn（对接受请求的块设备而言，块设备分页），行内容表示ppn（对于文件而言，文件分页）。
	#define PAGE_TABLE "/tmp/page_info"      //页信息文件。行号表示ppn（对于文件而言，文件分页），行内容表示页的状态，0表示空闲页，1表示有效页，2表示无效页。
	//本想使用位段的方式使用一个unsigned long型变量来表示ppn和页信息，但是位段不支持大于一个字节8位的位域

	//映射表和页信息的结构体，在内存的表示
	struct page_info {
		unsigned int ppn;        //对于这一成员来说，数组下标号对应lpn，用于lpn_to_ppn。
		unsigned int tag;        //对于这一成员来说，数组下标号对应ppn，用于标示物理页的状态（0表示空闲页，1表示有效页，2表示无效页）。
	};

	//空闲页链表
	struct freePage{
		unsigned int ppn;          //空闲页号
		struct list_head list;     //构建链表
	};

	//无效页链表
	struct invalidPage{
		unsigned int ppn;          //无效页号
		struct list_head list;     //构建链表
	};

	//定义链表头
	static struct freePage free_head; 
	static struct invalidPage invalid_head;
	
	//默认设置20个页大小（由于表的行内容初始化默认为0，废弃page 0不用）
	#define MAX_PAGE_NUM  20    // nsectors / 8
	static struct page_info pageInfo[MAX_PAGE_NUM];    //ppn成员默认都是0，由于该值表示ppn，所以将ppn为0的页废弃不用
	
##2. 关键的函数

	//重置表内容，将MAP_TABLE和PAGE_TABLE的行内容都置为0
	static int resetTable(void);
	
	//向内存载入PAGE_TABLE的内容，使用数组结构组织（下标表示物理页号），并将空闲页组织成空闲页链表，将无效页组织成无效页链表
	static int create_pageTable(void);
	
	//向内存载入映射表信息，使用数组结构组织（下标表示逻辑页号）
	static int create_mapTable(void);
	
	//将在内存中更新了的物理页信息表写回到文件PAGE_TABLE
	static int writeBack_pageTable(void);
	
	//将在内存中更新了的映射表写回到文件MAP_TABLE
	static int writeBack_mapTable(void);
	
	//简单的垃圾回收函数：当没有空闲页时进行垃圾页回收，一次回收掉所有无效页
	static void gcPage(void);
	
	//当进行写操作时，根据逻辑页号分配空闲页，从空闲链表获取1个空闲页，返回页号
	static unsigned int allocPage(void);
	
	//基本的写操作：内核空间中写文件
	//写入内容在w_buff中，ppn为要写入的物理页号，size为写入块大小，offset为页内偏移
	static int file_write(char *w_buf, size_t size, unsigned int ppn, loff_t offset);
	
	//基本的读操作：
	//内核空间中读文件，内容读到r_buff中，ppn为数据所在的物理页号，size为读取块大小，offset为页内偏移
	static int file_read(char *r_buf, size_t size, unsigned int ppn, loff_t offset);
	
	//处理由文件系统生成的I/O请求，读或者写
	//sector为要传输数据的起始扇区号，nsect为该次请求所要传输的扇区数
	//该函数将每一个I/O请求分解为以页为单位的读写操作，交给file_write和file_write执行
	static void bdev_transfer(struct my_bdev *dev, unsigned long sector, unsigned long nsect, char *buffer, int write);
	
#对程序进行测试

##1. insmod过程中执行了两个函数：	

	//初始化表文件，写入MAX_PAGE_NUM行0
	resetTable();
	//将表载入内存
	createTable();
	
dmesg信息如下：

	//===========================================================================
	[36069.261648] reset.
	[36069.261805] reset finished.
	[36069.261809] create_pageTable.      //在内存中创建空闲页的链表，遍历链表打印出以下信息。page 0废弃不用。
	[36069.261819] free page 1 
	[36069.261822] free page 2 
	[36069.261824] free page 3 
	[36069.261827] free page 4 
	[36069.261829] free page 5 
	[36069.261831] free page 6 
	[36069.261833] free page 7 
	[36069.261836] free page 8 
	[36069.261838] free page 9 
	[36069.261841] free page 10 
	[36069.261843] free page 11 
	[36069.261845] free page 12 
	[36069.261848] free page 13 
	[36069.261850] free page 14 
	[36069.261853] free page 15 
	[36069.261855] free page 16 
	[36069.261857] free page 17 
	[36069.261859] free page 18 
	[36069.261862] free page 19 
	[36069.261864] the number of the page table entries: 20
	[36069.261866] create_mapTable.           //在内存中建立映射表，使用数组数据结构
	[36069.261871] lpn     0   :   ppn     0
	[36069.261874] lpn     1   :   ppn     0
	[36069.261877] lpn     2   :   ppn     0
	[36069.261879] lpn     3   :   ppn     0
	[36069.261882] lpn     4   :   ppn     0
	[36069.261884] lpn     5   :   ppn     0
	[36069.261887] lpn     6   :   ppn     0
	[36069.261889] lpn     7   :   ppn     0
	[36069.261892] lpn     8   :   ppn     0
	[36069.261894] lpn     9   :   ppn     0
	[36069.261897] lpn    10   :   ppn     0
	[36069.261899] lpn    11   :   ppn     0
	[36069.261902] lpn    12   :   ppn     0
	[36069.261904] lpn    13   :   ppn     0
	[36069.261907] lpn    14   :   ppn     0
	[36069.261909] lpn    15   :   ppn     0
	[36069.261912] lpn    16   :   ppn     0
	[36069.261914] lpn    17   :   ppn     0
	[36069.261917] lpn    18   :   ppn     0
	[36069.261919] lpn    19   :   ppn     0
	[36069.261921] the number of the map table entries: 20
	//===========================================================================

##2. 写测试：

###(1)  dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=9

	//向磁盘头部连续写入9个sector的随机数据
	//引发了两个写操作，每次操作写一页，此时数据文件：
	File: ‘/tmp/blkdev_file’
	Size: 12288           Blocks: 16         IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 147366096   Links: 1

	//12288表示文件大小3个页，16表示占了16个块（每块512B）。
	//写了page 1和page 2,page 0废弃不用。
	
dmesg信息：

	//============================================================================
	//allocPage 1 ： 分配空闲页1
	//====================================
	[36076.835665] begin of bdev_transfer
	[36076.835666] allocPage. 
	[36076.835666] the table of free pages:      //分配之前打印出空闲页链表，除去page 0共19个
	[36076.835667] free page: 1 
	[36076.835668] free page: 2 
	[36076.835669] free page: 3 
	[36076.835670] free page: 4 
	[36076.835670] free page: 5 
	[36076.835671] free page: 6 
	[36076.835672] free page: 7 
	[36076.835672] free page: 8 
	[36076.835673] free page: 9 
	[36076.835674] free page: 10 
	[36076.835674] free page: 11 
	[36076.835675] free page: 12 
	[36076.835676] free page: 13 
	[36076.835676] free page: 14 
	[36076.835677] free page: 15 
	[36076.835678] free page: 16 
	[36076.835678] free page: 17 
	[36076.835679] free page: 18 
	[36076.835680] free page: 19 
	[36076.835681] delete free page: 1 in the free table     //在空闲页链表中删除这次分配的页节点，并置该节点的标识为1，即有效页
	[36076.835681] writeBack_pageTable.                      //更新页信息表
	[36076.835686] ppn : 0.
	[36076.835702] ppn : 1.      //这次分配使用的有效页为page 1
	[36076.835703] ppn : 0.
	[36076.835704] ppn : 0.
	[36076.835705] ppn : 0.
	[36076.835707] ppn : 0.
	[36076.835708] ppn : 0.
	[36076.835709] ppn : 0.
	[36076.835710] ppn : 0.
	[36076.835711] ppn : 0.
	[36076.835712] ppn : 0.
	[36076.835714] ppn : 0.
	[36076.835715] ppn : 0.
	[36076.835716] ppn : 0.
	[36076.835717] ppn : 0.
	[36076.835718] ppn : 0.
	[36076.835719] ppn : 0.
	[36076.835721] ppn : 0.
	[36076.835722] ppn : 0.
	[36076.835723] ppn : 0.
	[36076.835724] end of allocPage. 
	[36076.835725] file_write: the ppn is 1 (the lpn is 0).
	[36076.835728] file_write:write position: page_num: 1
	[36076.835745] file_write:write size: 4096
	[36076.835746] writeBack_mapTable.              //写完数据后，更新映射表。ppn=0的页废弃不用。
	[36076.835754] lpn     0   :   ppn    1         //更新
	[36076.835755] lpn     1   :   ppn    0
	[36076.835757] lpn     2   :   ppn    0
	[36076.835758] lpn     3   :   ppn    0
	[36076.835759] lpn     4   :   ppn    0
	[36076.835760] lpn     5   :   ppn    0
	[36076.835762] lpn     6   :   ppn    0
	[36076.835764] lpn     7   :   ppn    0
	[36076.835765] lpn     8   :   ppn    0
	[36076.835766] lpn     9   :   ppn    0
	[36076.835767] lpn    10   :   ppn    0
	[36076.835769] lpn    11   :   ppn    0
	[36076.835770] lpn    12   :   ppn    0
	[36076.835771] lpn    13   :   ppn    0
	[36076.835772] lpn    14   :   ppn    0
	[36076.835774] lpn    15   :   ppn    0
	[36076.835775] lpn    16   :   ppn    0
	[36076.835776] lpn    17   :   ppn    0
	[36076.835777] lpn    18   :   ppn    0
	[36076.835779] lpn    19   :   ppn    0
	[36076.835780] bdev_transfer:write file: 4096
	[36076.835780] end of bdev_transfer
	//============================================================================	
	//allocPage 2 ： 分配空闲页2
	//====================================	
	[36076.835790] allocPage. 
	[36076.835791] the table of free pages:       //空闲页链表少了 free page 1
	[36076.835792] free page: 2 
	[36076.835792] free page: 3 
	[36076.835793] free page: 4 
	[36076.835794] free page: 5 
	[36076.835794] free page: 6 
	[36076.835795] free page: 7 
	[36076.835796] free page: 8 
	[36076.835796] free page: 9 
	[36076.835797] free page: 10 
	[36076.835798] free page: 11 
	[36076.835798] free page: 12 
	[36076.835799] free page: 13 
	[36076.835800] free page: 14 
	[36076.835800] free page: 15 
	[36076.835801] free page: 16 
	[36076.835802] free page: 17 
	[36076.835802] free page: 18 
	[36076.835803] free page: 19 
	[36076.835804] delete free page: 2 in the free table    //这次分配的是page 2，从空闲页链表中删除该节点
	[36076.835804] writeBack_pageTable.
	[36076.835807] ppn : 0.
	[36076.835809] ppn : 1.        //有效页
	[36076.835810] ppn : 1.        //该次分配使用的有效页为page 2
	[36076.835811] ppn : 0.
	[36076.835812] ppn : 0.
	[36076.835813] ppn : 0.
	[36076.835814] ppn : 0.
	[36076.835816] ppn : 0.
	[36076.835817] ppn : 0.
	[36076.835818] ppn : 0.
	[36076.835819] ppn : 0.
	[36076.835820] ppn : 0.
	[36076.835821] ppn : 0.
	[36076.835822] ppn : 0.
	[36076.835824] ppn : 0.
	[36076.835825] ppn : 0.
	[36076.835826] ppn : 0.
	[36076.835827] ppn : 0.
	[36076.835828] ppn : 0.
	[36076.835829] ppn : 0.
	[36076.835831] end of allocPage. 
	[36076.835871] file_write: the ppn is 2 (the lpn is 1).
	[36076.835878] file_write:write position: page_num: 2
	[36076.835886] file_write:write size: 4096
	[36076.835887] writeBack_mapTable.             //映射表得到更新
	[36076.835890] lpn     0   :   ppn    1         
	[36076.835892] lpn     1   :   ppn    2        //更新
	[36076.835893] lpn     2   :   ppn    0
	[36076.835894] lpn     3   :   ppn    0
	[36076.835895] lpn     4   :   ppn    0
	[36076.835896] lpn     5   :   ppn    0
	[36076.835898] lpn     6   :   ppn    0
	[36076.835899] lpn     7   :   ppn    0
	[36076.835900] lpn     8   :   ppn    0
	[36076.835901] lpn     9   :   ppn    0
	[36076.835902] lpn    10   :   ppn    0
	[36076.835904] lpn    11   :   ppn    0
	[36076.835905] lpn    12   :   ppn    0
	[36076.835906] lpn    13   :   ppn    0
	[36076.835907] lpn    14   :   ppn    0
	[36076.835909] lpn    15   :   ppn    0
	[36076.835910] lpn    16   :   ppn    0
	[36076.835911] lpn    17   :   ppn    0
	[36076.835912] lpn    18   :   ppn    0
	[36076.835914] lpn    19   :   ppn    0
	[36076.835914] bdev_transfer:write file: 4096
	[36076.835915] end of bdev_transfer
	//============================================================================
	
###(2)  紧接着：dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=1

	//在磁盘头覆盖写一个扇区，此时文件：
    File: ‘/tmp/blkdev_file’
    Size: 16384           Blocks: 120        IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 147366096   Links: 1

看dmesg，可以发现在文件中不是覆盖写（写的是page 3）：

	//============================================================================
	[36994.948288] begin of bdev_transfer
	[36994.948290] allocPage.
	[36994.948291] the table of free pages:         //空闲页少了page 1和page 2
	[36994.948293] free page: 3            
	[36994.948294] free page: 4
	[36994.948296] free page: 5
	[36994.948298] free page: 6
	[36994.948299] free page: 7
	[36994.948300] free page: 8
	[36994.948302] free page: 9
	[36994.948303] free page: 10
	[36994.948305] free page: 11
	[36994.948306] free page: 12
	[36994.948308] free page: 13
	[36994.948309] free page: 14
	[36994.948310] free page: 15
	[36994.948312] free page: 16
	[36994.948313] free page: 17
	[36994.948315] free page: 18
	[36994.948316] free page: 19
	[36994.948318] delete free page: 3 in the free table       //这次分配的是page 3，从空闲页链表中删除该节点
	[36994.948319] end of allocPage.
	[36994.948539] writeBack_mapTable.               //映射表得到更新
	[36994.948661] lpn     0   :   ppn    3          //更新
	[36994.948667] lpn     1   :   ppn    2
	[36994.948670] lpn     2   :   ppn    0
	[36994.948673] lpn     3   :   ppn    0
	[36994.948675] lpn     4   :   ppn    0
	[36994.948678] lpn     5   :   ppn    0
	[36994.948680] lpn     6   :   ppn    0
	[36994.948683] lpn     7   :   ppn    0
	[36994.948686] lpn     8   :   ppn    0
	[36994.948688] lpn     9   :   ppn    0
	[36994.948691] lpn    10   :   ppn    0
	[36994.948875] lpn    11   :   ppn    0
	[36994.948883] lpn    12   :   ppn    0
	[36994.948886] lpn    13   :   ppn    0
	[36994.948889] lpn    14   :   ppn    0
	[36994.948891] lpn    15   :   ppn    0
	[36994.948894] lpn    16   :   ppn    0
	[36994.948897] lpn    17   :   ppn    0
	[36994.948899] lpn    18   :   ppn    0
	[36994.948902] lpn    19   :   ppn    0
	[37770.670087] writeBack_pageTable.         //page_table得到更新
	[37770.670093] ppn : 0.                     
	[37770.670099] ppn : 2.						//将旧的ppn置为无效
	[37770.670102] ppn : 1.                     //有效页
	[37770.670105] ppn : 1.                     //新的有效页
	[37770.670107] ppn : 0.
	[37770.670109] ppn : 0.
	[37770.670112] ppn : 0.
	[37770.670114] ppn : 0.
	[37770.670117] ppn : 0.
	[37770.670119] ppn : 0.
	[37770.670122] ppn : 0.
	[37770.670124] ppn : 0.
	[37770.670127] ppn : 0.
	[37770.670129] ppn : 0.
	[37770.670132] ppn : 0.
	[37770.670134] ppn : 0.
	[37770.670136] ppn : 0.
	[37770.670139] ppn : 0.
	[37770.670141] ppn : 0.
	[37770.670144] ppn : 0.
	[36994.948905] bdev_transfer:write file: 4096
	[36994.948906] end of bdev_transfer
	//============================================================================
	
###(3)  dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=4 seek=16
	
seek=16 表示从磁盘的第17个扇区开始写起，即lpn=2，dmesg信息如下：
	
	//============================================================================
	[38007.424065] begin of bdev_transfer
	[38007.424065] allocPage.
	[38007.424065] the table of free pages:         //空闲页少了page 1,page 2,page 3
	[38007.424065] free page: 4
	[38007.424065] free page: 5
	[38007.424065] free page: 6
	[38007.424065] free page: 7
	[38007.424065] free page: 8
	[38007.424065] free page: 9
	[38007.424065] free page: 10
	[38007.424065] free page: 11
	[38007.424065] free page: 12
	[38007.424065] free page: 13
	[38007.424065] free page: 14
	[38007.424065] free page: 15
	[38007.424065] free page: 16
	[38007.424065] free page: 17
	[38007.424065] free page: 18
	[38007.424065] free page: 19
	[38007.424065] delete free page: 4 in the free table       //这次分配的是page 4，从空闲页链表中删除该节点
	[38007.424065] end of allocPage.
	[38007.424065] file_write: the ppn is 4 (the lpn is 2).
	[38007.424065] file_write:write position: page_num: 4      //这次写的是page 4
	[38007.424065] file_write:write size: 4096
	[38007.424289] writeBack_mapTable.                //映射表得到更新
	[38007.424321] lpn     0   :   ppn    3
	[38007.424418] lpn     1   :   ppn    2
	[38007.424431] lpn     2   :   ppn    4           //更新
	[38007.424435] lpn     3   :   ppn    0
	[38007.424438] lpn     4   :   ppn    0
	[38007.424440] lpn     5   :   ppn    0
	[38007.424443] lpn     6   :   ppn    0
	[38007.424446] lpn     7   :   ppn    0
	[38007.424448] lpn     8   :   ppn    0
	[38007.424451] lpn     9   :   ppn    0
	[38007.424454] lpn    10   :   ppn    0
	[38007.424457] lpn    11   :   ppn    0
	[38007.424460] lpn    12   :   ppn    0
	[38007.424462] lpn    13   :   ppn    0
	[38007.424465] lpn    14   :   ppn    0
	[38007.424468] lpn    15   :   ppn    0
	[38007.424470] lpn    16   :   ppn    0
	[38007.424473] lpn    17   :   ppn    0
	[38007.424476] lpn    18   :   ppn    0
	[38007.424478] lpn    19   :   ppn    0
	[38007.424480] writeBack_pageTable.
	[38007.424487] ppn : 0.            
	[38007.424505] ppn : 2.				//无效页
	[38007.424508] ppn : 1.				//有效页
	[38007.424510] ppn : 1.				//有效页
	[38007.424513] ppn : 1.				//这次分配使用的有效页
	[38007.424515] ppn : 0.
	[38007.424518] ppn : 0.
	[38007.424520] ppn : 0.
	[38007.424522] ppn : 0.
	[38007.424525] ppn : 0.
	[38007.424527] ppn : 0.
	[38007.424530] ppn : 0.
	[38007.424532] ppn : 0.
	[38007.424535] ppn : 0.
	[38007.424537] ppn : 0.
	[38007.424539] ppn : 0.
	[38007.424542] ppn : 0.
	[38007.424544] ppn : 0.
	[38007.424547] ppn : 0.
	[38007.424549] ppn : 0.
	[38007.424552] bdev_transfer:write file: 4096
	[38007.424554] end of bdev_transfer
	//=============================================================


###(4)  dd if=/dev/urandom of=/dev/my_bdeva bs=160k count=1          //设备只有20页，80K

    提示出错：//error: not enough free page.

##垃圾回收测试（卸载模块，删除文件，并重新加载模块，以便测试）：

###(1)   dd if=/dev/urandom of=/dev/my_bdeva bs=4k count=1    连续5次

连续对块设备头写五次（每次写4KB）之后，dmesg：

	//========================================================================
	[38420.917152] begin of bdev_transfer
	[38420.917154] allocPage.
	[38420.917155] the table of free pages:          //前四页已经使用
	[38420.917157] free page: 5
	[38420.917159] free page: 6
	[38420.917161] free page: 7
	[38420.917162] free page: 8
	[38420.917163] free page: 9
	[38420.917165] free page: 10
	[38420.917166] free page: 11
	[38420.917168] free page: 12
	[38420.917169] free page: 13
	[38420.917171] free page: 14
	[38420.917172] free page: 15
	[38420.917174] free page: 16
	[38420.917175] free page: 17
	[38420.917177] free page: 18
	[38420.917178] free page: 19
	[38420.917180] delete free page: 5 in the free table        //这次分配page 5
	[38420.917182] end of allocPage. 
	[38420.917184] file_write: the ppn is 5 (the lpn is 0).
	[38420.917192] file_write:write position: page_num: 5
	[38420.917225] file_write:write size: 4096
	[38420.917228] writeBack_mapTable.
	[38420.917286] lpn     0   :   ppn    5              //前四页为无效，使用的是第五页
	[38420.917289] lpn     1   :   ppn    0
	[38420.917292] lpn     2   :   ppn    0
	[38420.917295] lpn     3   :   ppn    0
	[38420.917298] lpn     4   :   ppn    0
	[38420.917300] lpn     5   :   ppn    0
	[38420.917303] lpn     6   :   ppn    0
	[38420.917305] lpn     7   :   ppn    0
	[38420.917308] lpn     8   :   ppn    0
	[38420.917311] lpn     9   :   ppn    0
	[38420.917314] lpn    10   :   ppn    0
	[38420.917316] lpn    11   :   ppn    0
	[38420.917319] lpn    12   :   ppn    0
	[38420.917322] lpn    13   :   ppn    0
	[38420.917324] lpn    14   :   ppn    0
	[38420.917327] lpn    15   :   ppn    0
	[38420.917330] lpn    16   :   ppn    0
	[38420.917332] lpn    17   :   ppn    0
	[38420.917335] lpn    18   :   ppn    0
	[38420.917338] lpn    19   :   ppn    0
	[38420.917340] writeBack_pageTable.
	[38420.917345] ppn : 0.                             
	[38420.917353] ppn : 2.                     //前四页为无效，使用的是第五页
	[38420.917355] ppn : 2.
	[38420.917358] ppn : 2.
	[38420.917360] ppn : 2.
	[38420.917363] ppn : 1.
	[38420.917365] ppn : 0.
	[38420.917368] ppn : 0.
	[38420.917370] ppn : 0.
	[38420.917372] ppn : 0.
	[38420.917375] ppn : 0.
	[38420.917377] ppn : 0.
	[38420.917380] ppn : 0.
	[38420.917382] ppn : 0.
	[38420.917385] ppn : 0.
	[38420.917387] ppn : 0.
	[38420.917389] ppn : 0.
	[38420.917392] ppn : 0.
	[38420.917394] ppn : 0.
	[38420.917397] ppn : 0.
	[38420.917400] bdev_transfer:write file: 4096
	[38420.917401] end of bdev_transfer
	//========================================================================



###(2)  dd if=/dev/urandom of=/dev/my_bdeva bs=4k count=1    重复执行直到写满文件

将块设备写满，dmesg：

	//========================================================================
	[38552.050891] writeBack_mapTable.
	[38552.050961] lpn     0   :   ppn   19            //重复对lpn=0（块设备头）写入，对应的ppn号（文件中的页号）由1变换到19
	[38552.050966] lpn     1   :   ppn    0
	[38552.050969] lpn     2   :   ppn    0
	[38552.050971] lpn     3   :   ppn    0
	[38552.050974] lpn     4   :   ppn    0
	[38552.050977] lpn     5   :   ppn    0
	[38552.050980] lpn     6   :   ppn    0
	[38552.050982] lpn     7   :   ppn    0
	[38552.050985] lpn     8   :   ppn    0
	[38552.050987] lpn     9   :   ppn    0
	[38552.050990] lpn    10   :   ppn    0
	[38552.050993] lpn    11   :   ppn    0
	[38552.050996] lpn    12   :   ppn    0
	[38552.050999] lpn    13   :   ppn    0
	[38552.051001] lpn    14   :   ppn    0
	[38552.051004] lpn    15   :   ppn    0
	[38552.051007] lpn    16   :   ppn    0
	[38552.051009] lpn    17   :   ppn    0
	[38552.051012] lpn    18   :   ppn    0
	[38552.051015] lpn    19   :   ppn    0
	[38552.051017] writeBack_pageTable.                 //文件只有19页为有效页，其他都是无效页（旧数据）
	[38552.051024] ppn : 0.
	[38552.051033] ppn : 2.
	[38552.051036] ppn : 2.
	[38552.051038] ppn : 2.
	[38552.051041] ppn : 2.
	[38552.051043] ppn : 2.
	[38552.051046] ppn : 2.
	[38552.051048] ppn : 2.
	[38552.051050] ppn : 2.
	[38552.051053] ppn : 2.
	[38552.051055] ppn : 2.
	[38552.051057] ppn : 2.
	[38552.051060] ppn : 2.
	[38552.051062] ppn : 2.
	[38552.051065] ppn : 2.
	[38552.051067] ppn : 2.
	[38552.051069] ppn : 2.
	[38552.051072] ppn : 2.
	[38552.051074] ppn : 2.
	[38552.051077] ppn : 1.
	//========================================================================

###(3)  dd if=/dev/urandom of=/dev/my_bdeva bs=4k count=1 

由于已经没有空闲页，启动垃圾回收程序，dmesg：

	//========================================================================
	[  261.290627] begin of bdev_transfer
	[  261.290629] allocPage.
	[  261.290630] ///////////////////////////////gcPage.       //启动垃圾回收函数，将所有无效页变为空闲页
	[  261.290632] the table of invalid pages:                  //遍历无效页链表，逐个删除无效页节点，并建立相应的空闲页节点加到空闲页链表中
	[  261.290634] invalid page: 1
	[  261.290636] delete invalid page: 1
	[  261.290637] now is free page: 1
	[  261.290639] invalid page: 2
	[  261.290640] delete invalid page: 2
	[  261.290642] now is free page: 2
	[  261.290643] invalid page: 3
	[  261.290645] delete invalid page: 3
	[  261.290646] now is free page: 3
	[  261.290647] invalid page: 4
	[  261.290649] delete invalid page: 4
	[  261.290650] now is free page: 4
	[  261.290652] invalid page: 5
	[  261.290653] delete invalid page: 5
	[  261.290654] now is free page: 5
	[  261.290656] invalid page: 6
	[  261.290657] delete invalid page: 6
	[  261.290659] now is free page: 6
	[  261.290660] invalid page: 7
	[  261.290662] delete invalid page: 7
	[  261.290663] now is free page: 7
	[  261.290664] invalid page: 8
	[  261.290666] delete invalid page: 8
	[  261.290667] now is free page: 8
	[  261.290669] invalid page: 9
	[  261.290670] delete invalid page: 9
	[  261.290671] now is free page: 9
	[  261.290673] invalid page: 10
	[  261.290674] delete invalid page: 10
	[  261.290676] now is free page: 10
	[  261.290677] invalid page: 11
	[  261.290679] delete invalid page: 11
	[  261.290680] now is free page: 11
	[  261.290682] invalid page: 12
	[  261.290683] delete invalid page: 12
	[  261.290685] now is free page: 12
	[  261.290686] invalid page: 13
	[  261.290687] delete invalid page: 13
	[  261.290689] now is free page: 13
	[  261.290690] invalid page: 14
	[  261.290692] delete invalid page: 14
	[  261.290693] now is free page: 14
	[  261.290695] invalid page: 15
	[  261.290696] delete invalid page: 15
	[  261.290697] now is free page: 15
	[  261.290699] invalid page: 16
	[  261.290700] delete invalid page: 16
	[  261.290702] now is free page: 16
	[  261.290703] invalid page: 17
	[  261.290705] delete invalid page: 17
	[  261.290706] now is free page: 17
	[  261.290707] invalid page: 18
	[  261.290709] delete invalid page: 18
	[  261.290710] now is free page: 18
	[  261.290712] writeBack_pageTable.                 //垃圾回收之后的页信息表，1到18页均已回收
	[  261.290722] ppn : 0.
	[  261.290738] ppn : 0.
	[  261.290741] ppn : 0.
	[  261.290743] ppn : 0.
	[  261.290745] ppn : 0.
	[  261.290748] ppn : 0.
	[  261.290750] ppn : 0.
	[  261.290752] ppn : 0.
	[  261.290755] ppn : 0.
	[  261.290757] ppn : 0.
	[  261.290759] ppn : 0.
	[  261.290762] ppn : 0.
	[  261.290764] ppn : 0.
	[  261.290766] ppn : 0.
	[  261.290769] ppn : 0.
	[  261.290771] ppn : 0.
	[  261.290773] ppn : 0.
	[  261.290776] ppn : 0.
	[  261.290778] ppn : 0.
	[  261.290780] ppn : 1.
	[  261.290783] the table of free pages:           //垃圾回收后，又有空闲页了！
	[  261.290785] free page: 1
	[  261.290786] free page: 2
	[  261.290787] free page: 3
	[  261.290789] free page: 4
	[  261.290790] free page: 5
	[  261.290791] free page: 6
	[  261.290793] free page: 7
	[  261.290794] free page: 8
	[  261.290795] free page: 9
	[  261.290796] free page: 10
	[  261.290798] free page: 11
	[  261.290799] free page: 12
	[  261.290801] free page: 13
	[  261.290802] free page: 14
	[  261.290803] free page: 15
	[  261.290805] free page: 16
	[  261.290806] free page: 17
	[  261.290807] free page: 18
	[  261.290809] delete free page: 1 in the free table      //给这次写入分配刚回收得到空闲页page 1
	[  261.290810] end of allocPage.
	[  261.290812] file_write: the ppn is 1 (the lpn is 0).
	[  261.290818] file_write:write position: page_num: 1
	[  261.290830] file_write:write size: 4096
	[  261.290832] writeBack_mapTable.                        //新的映射表
	[  261.290844] lpn     0   :   ppn    1
	[  261.290847] lpn     1   :   ppn    0
	[  261.290849] lpn     2   :   ppn    0
	[  261.290852] lpn     3   :   ppn    0
	[  261.290855] lpn     4   :   ppn    0
	[  261.290857] lpn     5   :   ppn    0
	[  261.290860] lpn     6   :   ppn    0
	[  261.290862] lpn     7   :   ppn    0
	[  261.290865] lpn     8   :   ppn    0
	[  261.290867] lpn     9   :   ppn    0
	[  261.290870] lpn    10   :   ppn    0
	[  261.290873] lpn    11   :   ppn    0
	[  261.290875] lpn    12   :   ppn    0
	[  261.290878] lpn    13   :   ppn    0
	[  261.290880] lpn    14   :   ppn    0
	[  261.290883] lpn    15   :   ppn    0
	[  261.290885] lpn    16   :   ppn    0
	[  261.290888] lpn    17   :   ppn    0
	[  261.290891] lpn    18   :   ppn    0
	[  261.290893] lpn    19   :   ppn    0
	[  261.290895] now page 19 is invalid.             //之前lpn=0对应的是ppn=19，现在lpn=0对应ppn=1，所以ppn=19的页变为无效
	[  261.290897] writeBack_pageTable.
	[  261.290925] ppn : 0.
	[  261.290931] ppn : 1.
	[  261.290933] ppn : 0.
	[  261.290936] ppn : 0.
	[  261.290938] ppn : 0.
	[  261.290940] ppn : 0.
	[  261.290942] ppn : 0.
	[  261.290945] ppn : 0.
	[  261.290947] ppn : 0.
	[  261.290949] ppn : 0.
	[  261.290951] ppn : 0.
	[  261.290954] ppn : 0.
	[  261.290956] ppn : 0.
	[  261.290958] ppn : 0.
	[  261.290960] ppn : 0.
	[  261.290963] ppn : 0.
	[  261.290965] ppn : 0.
	[  261.290967] ppn : 0.
	[  261.290970] ppn : 0.
	[  261.290972] ppn : 2.                                  //page 19无效
	[  261.290975] bdev_transfer:write file: 4096
	[  261.290976] end of bdev_transfer
	//========================================================================
	
#总结：

1. 内核空间的编程与用户空间下的编程区别很大。

用户空间下的函数库（stdio.h, stdlib.h中定义的函数）在内核空间下不能使用，不过对于其中的大部分函数，内核空间都有自己对应的实现，其他的只能自己想办法实现。

比如：内核空间下没有atoul(内核可使用simple_strtoul),itoa(内核可使用sprintf),fgets(自己写函数实现)等；内核空间有属于自己构建和使用链表的方式—— list_head。

这需要在写程序勤查资料：google和linux内核源代码。

2. 对一个程序的各个功能模块一定要划分清楚，不然会给后期调试带来巨大麻烦。

这必须要对程序的功能很清楚，在前期清晰地设计好各功能模块。