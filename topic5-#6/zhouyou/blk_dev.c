//#include <linux/config.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/kernel.h>   // printk() 
#include <linux/slab.h>     // kmalloc() 
#include <linux/fs.h>       // everything... 
#include <linux/errno.h>    // error codes 
#include <linux/timer.h>
#include <linux/types.h>    // size_t 
#include <linux/fcntl.h>    // O_ACCMODE 
#include <linux/hdreg.h>    // HDIO_GETGEO 
#include <linux/kdev_t.h>
#include <linux/vmalloc.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/buffer_head.h>   // invalidate_bdev 
#include <linux/bio.h>
#include<linux/string.h>  
#include<asm/uaccess.h>       //get_fs(),set_fs(),get_ds()
#include<linux/list.h>

//#include<stdlib.h>    stdlib.h是用户空间的库，内核空间不能够使用。    
//sprintf(buf, "%lu", n)用于将unsigned long型整数转换为字符串；unsigned long strtoul(buf, NULL, 0)用于将字符串转换为unsigned long型(或unsigned int型)

#define FILE_DIR "/tmp/blkdev_file"      //数据文件将设备数据映射到某个文件，定义数据文件的路径
#define MAP_TABLE "/tmp/map_file"        //页表文件，将映射表映射到某个文件，定义映射表文件的路径。行号表示lpn，行内容表示ppn（unsigned int型）。
#define PAGE_TABLE "/tmp/page_info"       //页信息，0表示空闲页，1表示有效页，2表示无效页（unsigned int型）
//本想使用位段的方式使用一个ul型变量来表示ppn和页信息，但是位段不支持大于一个字节8位的位域

typedef struct request_queue request_queue_t;        

MODULE_LICENSE("Dual BSD/GPL");

//===================================================================================================

//在用户态下编程可以通过main()的来传递命令行参数，而编写一个内核模块则通过module_param()
//参数用 module_param 宏定义来声明, 它定义在 moduleparam.h

static int bdev_major = 0;           //主设备号默认设为0，表示动态分配
module_param(bdev_major, int, 0);
static int hardsect_size = 512;      //请求队列描述符中的字段，表示扇区大小512B
module_param(hardsect_size, int , 0);
static int nsectors = 160;          //块设备的大小,80KB,20个页  
module_param(nsectors, int, 0);
static int ndevices = 1;        
module_param(ndevices, int, 0);

//不同的 request mode
enum {
	RM_SIMPLE = 0,          //简单模式，使用简单的请求处理函数
	RM_FULL = 1,           //完全模式，使用了bio
	RM_NOQUEUE = 2,        //使用make_request，不使用请求队列
};
static int request_mode = RM_NOQUEUE;      // 这里选择make_request  //
module_param(request_mode, int, 0);


//次设备号与分区管理
/*
现在的Linux使用了定义在<linxu/kdev_t.h>中的一个新类型kdev_t来保存设备类型。
MAJOR(kdev_t dev)从kdev_t结构中获取主设备号
MINOR(kdev_t dev)得到次设备号
MKDEV(int ma,int mi) 通过主设备号和次设备号创建kdev_t
kdev_t_to_nr(kdev_t_dev) 将kdev_t转化为一个整数（一个dev_t)
*/
#define BDEV_MINORS 16
#define MINOR_SHIFT   4
#define DEVNUM(kdevnum)  (MINIOR(kdev_t_to_nr(kdevnum)) >> MINOR_SHIFT

//我们可以修改我们的设备的扇区大小，但是内核依然认为一个扇区是512B
#define KERNEL_SECTOR_SIZE 512

// After this much idle time, the driver will simulate a media change.
#define INVALIDATE_DELAY 30*HZ

//描述块设备的结构体
struct my_bdev {
	int size;                       //设备大小，以扇区为单位
	u8 *data;                       //数据数组
	short users;                    //用户数目
	short media_change;             //介质改变标志
	spinlock_t lock;                //用于互斥
	request_queue_t *queue;         //设备请求队列
	struct gendisk *gd;             //gendisk结构
	struct timer_list timer;        //用来模拟介质改变
};
//这就是我们硬盘的数据结构，有该硬盘在内核的表示gd, 硬盘的操作请求队列queue，
//设备操作自旋锁，以及操作延时的定时器,通过定时器模拟一个可更换介质的块设备
static struct my_bdev *Devices = NULL;

//页表和页信息的结构体，在内存的表示
struct page_info {
	unsigned int ppn;        //对于这一成员来说，数组下标号对应lpn，用于lpn_to_ppn
	unsigned int tag;        //对于这一成员来说，数组下标号对应ppn，用于标示物理页的状态（0表示空闲页，1表示有效页，2表示无效页）
};

//空闲页链表
struct freePage{
	unsigned int ppn;         //空闲页号
	struct list_head list;     //构建链表
};

//无效页链表
struct invalidPage{
	unsigned int ppn;         //无效页号
	struct list_head list;     //构建链表
};

//定义链表头
static struct freePage free_head; 
static struct invalidPage invalid_head;

#define MAX_PAGE_NUM  20    // nsectors / 8
static struct page_info pageInfo[MAX_PAGE_NUM];    //ppn成员默认都是0，由于该值表示ppn，所以将ppn为0的页废弃不用
	
//=================================================================================================
//关于页表和PAGE_TABLE的处理函数
//================================================================================================

//===============================
//重置表内容，行内容都为0
//resetTable
//===============================
static int resetTable(void)
{
	struct file *filp_page;
	struct file *filp_map;
	mm_segment_t fs;
	int i;
	char s[] = "0\n";
	
	printk("reset.\n");
	filp_page = filp_open(PAGE_TABLE, O_RDWR | O_CREAT, 0); 
	filp_map = filp_open(MAP_TABLE, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp_page) || IS_ERR(filp_map) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	fs = get_fs();
	set_fs(KERNEL_DS);

	filp_page->f_op->llseek(filp_page, 0, 0);
	filp_map->f_op->llseek(filp_map, 0, 0);
	
	for(i=0; i < MAX_PAGE_NUM; i++)	
	{
		filp_page->f_op->write(filp_page, s, strlen(s), &(filp_page->f_pos)); 
		filp_map->f_op->write(filp_map, s, strlen(s), &(filp_map->f_pos)); 
	}
	printk("reset finished.\n");
	
	set_fs(fs);
	filp_close(filp_page, NULL); 
	filp_close(filp_map, NULL); 
	return 0;
}

//=================================================


//================================================
//向内存中载入表信息
//createTable : create_pageTable + create_mapTable
//================================================

//内核中不能使用用户空间函数库，比如fgets、atoi之类定义在<stdlib.h>的函数
//自己实现一次从文件读一行的函数，遇到回车或者非‘0’~‘9’的字符退出函数
static int filegets(char *buf, size_t size, struct file *filp)
{
	char tmp;
	int i = 0;
	
	if(size == 0 || buf == NULL)
		return 0;
	
	while(size--)
	{	
		filp->f_op->read(filp, &tmp, 1, &(filp->f_pos));
		
		if(tmp == '\n')
		{	
			buf[i] = '\0';
			break;
		}
		
		if(tmp < '0' || tmp > '9')      //这里表示是读到了文件末尾（这里的文件存储的都是数字），使用EOF来判断不可行，暂时未发现有效的一般化方法
			return 0;
		
		buf[i++] = tmp;
	}
	return i;
}

//向内存载入物理页标识信息
static int create_pageTable(void)
{
	struct file *filp;
	mm_segment_t fs;
	
	struct freePage *freeNode;
	struct invalidPage *invalidNode;
	
	char buf[3];               //用于读取行内容
	int i = 0;
	int count = MAX_PAGE_NUM;
	
	printk("create_pageTable.\n");
	filp = filp_open(PAGE_TABLE, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	//初始化链表头  
    INIT_LIST_HEAD(&free_head.list); 
    INIT_LIST_HEAD(&invalid_head.list); 
	
	fs = get_fs();
	set_fs(KERNEL_DS);
	
	filp->f_op->llseek(filp, 0, 0);     //移动到文件头
	while( ( filegets(buf, 3, filp) != 0 ) && count-- )
	{
		pageInfo[i].tag = simple_strtoul(buf, NULL, 0);         //simple_strtoul为内核空间使用的转换函数
		if( ( i != 0 ) && ( pageInfo[i].tag == 0 ))                               //建立空闲页链表，废弃ppn为0的页
		{
			freeNode = (struct freePage *)kmalloc(sizeof(struct freePage), GFP_KERNEL); 
			freeNode->ppn = i;
			// 把这个节点链接到链表后面 
			list_add_tail( &(freeNode->list), &(free_head.list) );
			printk("free page %u \n", i);
			
		}
		if(pageInfo[i].tag == 1)
		{
			printk("valid page %u \n", i);
		}
		if(pageInfo[i].tag == 2)                               //建立无效页链表
		{
			invalidNode = (struct invalidPage*)kmalloc(sizeof(struct invalidPage), GFP_KERNEL); 
			invalidNode->ppn = i;
			// 把这个节点链接到链表后面 
			list_add_tail( &(invalidNode->list), &(invalid_head.list) );
			printk("invalid page %u \n", i);
		}
		i++;
	}
	printk("the number of the page table entries: %d\n",i);
	
	set_fs(fs);
	filp_close(filp, NULL); 
	return 0;
}

//向内存载入映射表信息
static int create_mapTable(void)
{
	struct file *filp;
	mm_segment_t fs;
	char buf[100];                   //用于读取行内容的缓冲区
	int i = 0;
	int count = MAX_PAGE_NUM;
	
	printk("create_mapTable.\n");
	filp = filp_open(MAP_TABLE, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	fs = get_fs();
	set_fs(KERNEL_DS);
	
	filp->f_op->llseek(filp, 0, 0);     //移动到文件头
	while( ( filegets(buf, 100, filp) != 0 ) && count-- )
	{
		pageInfo[i].ppn = simple_strtoul(buf, NULL, 0);         //simple_strtoul为内核空间使用的转换函数
		printk("lpn %5d   :   ppn %5u\n", i, pageInfo[i].ppn);
		i++;
	}
	printk("the number of the map table entries: %d\n",i);
	
	set_fs(fs);
	filp_close(filp, NULL); 
	return 0;
}

static void createTable(void)
{
	create_pageTable();
	create_mapTable();
}

//====================================================


//================================================
//向文件写回表内容
//writeBack : writeBack_pageTable + writeBack_mapTable
//================================================

//将在内存中更新了的物理页信息表写回到文件
static int writeBack_pageTable(void)
{
	struct file *filp;
	mm_segment_t fs;
	char s[]="0\n";
	int i;
	
	printk("writeBack_pageTable.\n");
	filp = filp_open(PAGE_TABLE, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	fs = get_fs();
	set_fs(KERNEL_DS);
	
	filp->f_op->llseek(filp, 0, 0);  //移到文件头
	for(i = 0; i < MAX_PAGE_NUM; i++)
	{
		s[0] += pageInfo[i].tag;      //需要写入文件第i行的内容
		printk("ppn : %u.\n", pageInfo[i].tag);
		filp->f_op->write(filp, s, strlen(s), &(filp->f_pos));      //写入文件
		s[0] = '0';
	}
	
	set_fs(fs);
	filp_close(filp, NULL); 
	return 0;
}

//将在内存中更新了的映射表写回到文件
static int writeBack_mapTable(void)
{
	struct file *filp;
	mm_segment_t fs;
	char s[100];             //记录行内容，ppn
	int i;
	int count;
	
	printk("writeBack_mapTable.\n");
	filp = filp_open(MAP_TABLE, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	fs = get_fs();
	set_fs(KERNEL_DS);
	
	filp->f_op->llseek(filp, 0, 0);  //移到文件头
	for(i = 0; i < MAX_PAGE_NUM; i++)
	{
		count = sprintf(s, "%u", pageInfo[i].ppn);      //记录写入s的字符数
		s[count] = '\n';
		s[count + 1] = '\0';
		filp->f_op->write(filp, s, count + 1, &(filp->f_pos));      //写入文件
		printk("lpn %5d   :   ppn %5s", i, s);
	}
	
	set_fs(fs);
	filp_close(filp, NULL); 
	return 0;
}

/*
static void writeBack(void)
{
	writeBack_pageTable();
	writeBack_mapTable();
}
*/

//===================================================
/*
//test：修改pageTable中第一行的数据为 1
static int testTable(void)
{
	pageInfo[1].tag = 1;
	pageInfo[2].tag = 2;
	pageInfo[1].ppn = 15;
	pageInfo[2].ppn = 9;
	
	printk("test_Table.\n");
	writeBack();
	return 0;
}
*/
//======================================
//链表相关操作：分配空闲页，垃圾页回收
//======================================

//当没有空闲页时进行垃圾页回收，一次回收掉所有无效页
static void gcPage(void)
{
	unsigned int ppn;
	struct list_head *pos, *pos_bak;
	struct freePage *freeNode;
	struct invalidPage *tmp;

	printk("///////////////////////////////////////gcPage.\n");
	//判断空闲页链表是否为空
	if( list_empty(&invalid_head.list) )    
	{
		printk("there is no invalid page. \n");
		return;
	}
	
	//遍历无效页链表，打印无效页表，并回收
	printk("the table of invalid pages:\n");
	list_for_each_safe(pos, pos_bak, &(invalid_head.list))                 //这里有问题！！！！无限循环
    { 															//解决问题：删除节点时遍历会断掉，使用list_for_each_safe即可。
        //这里我们用list_entry来取得pos所在的结构的指针 
        tmp = list_entry(pos, struct invalidPage, list);
		ppn = tmp->ppn;
        printk("invalid page: %u \n", ppn); 
		
		//删除每一个无效页节点
		list_del( &(tmp->list) );
		kfree(tmp);
		printk("delete invalid page: %u \n", ppn); 
		
		//建立对应的节点到空闲页链表末尾
		freeNode = (struct freePage *)kmalloc(sizeof(struct freePage), GFP_KERNEL);
		freeNode->ppn = ppn;
		list_add_tail( &(freeNode->list), &(free_head.list) );
		
		//将页信息置为空闲
		pageInfo[ppn].tag = 0;
		
		printk("now is free page: %u \n", ppn);
    } 
	writeBack_pageTable();
	
	return;
}


//分配空闲页，从空闲链表获取1个空闲页
static unsigned int allocPage(void)
{
	unsigned int ppn;
	struct list_head *pos;
	struct freePage *tmp;
	
	printk("allocPage. \n");
	//判断空闲链表是否为空
	if( list_empty(&free_head.list) )            //如果空闲链表为空进入
	{
		if( list_empty(&invalid_head.list) )     //再判断无效页表是否为空，如果为空，说明所有页都是有效页，文件已写满，没有空间了
		{
			printk("error: no more space. \n");
			return 0;         //物理0号页废弃不用
		}
		
		gcPage();           //如果空闲页链表为空的，但无效页链表不为空，进行垃圾回收
	}
	
	//遍历空闲页链表，打印空闲页表
	printk("the table of free pages:\n");
	list_for_each(pos, &free_head.list) 
    { 
        // 这里我们用list_entry来取得pos所在的结构的指针 
        tmp = list_entry(pos, struct freePage, list);
		ppn = tmp->ppn;
        printk("free page: %u \n", ppn); 
    } 
	 
	//分配空闲页
	tmp = list_entry( (free_head.list).next, struct freePage, list);     //链表首节点
	ppn = tmp->ppn;
	
	
		
	//从空闲页链表中删除分配出去的空闲页
	list_del( &(tmp->list) ); 
	kfree(tmp);
	printk("delete free page: %u in the free table\n", ppn); 
	
	printk("end of allocPage. \n");
	return ppn;
}


//=============================================

//=================================================================================================
//请求处理，传输数据的方式

/*写文件：首先打开文件，并进入内核空间*/
//ppn为文件上的物理页号，offset为页内偏移
static int file_write(char *w_buf, size_t size, unsigned int ppn, loff_t offset)
//static int file_write(char *w_buf)
{
	struct file *filp;       //文件指针

	mm_segment_t fs;    	
	loff_t w_pos;
	
	filp = filp_open(FILE_DIR, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	fs = get_fs();   
	//改变kernel对内存地址检查的处理方式，该函数的参数只有两个取值
	//USER_DS，KERNEL_DS，分别代表用户空间和内核空间，默认情况下，kernel取值为USER_DS
	set_fs(KERNEL_DS);
	
	//文件内的写位置:  页号*页大小(B) + 页内偏移
	w_pos = ppn * 4096 + offset;
	printk("file_write:write position: page_num: %u\n", ppn);
	
	//移动文件指针，指示写入位置，并写入
	//llseek(struct file *filp, loff_t offset, int origin)
	//offset：偏移量；origin：偏移量相对位置，0表示绝对位置，1表示相对当前位置   
	//filp->f_op->llseek(filp, 0, 0);   //设为从头写起
	filp->f_op->llseek(filp, w_pos, 0);
	filp->f_op->write(filp, w_buf, size, &(filp->f_pos));    
	printk("file_write:write size: %zu\n", size);
	
	//改变kernel对内存地址检查的处理方式为用户空间
	set_fs(fs);
	filp_close(filp, NULL); 
	return 0;
}

//读文件  ppn为文件上的物理页号
static int file_read(char *r_buf, size_t size, unsigned int ppn, loff_t offset)
{
	struct file *filp;       //文件指针	
	
	mm_segment_t fs;            
	loff_t r_pos;
	
	filp = filp_open(FILE_DIR, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	fs = get_fs();   
	//改变kernel对内存地址检查的处理方式，该函数的参数只有两个取值
	//USER_DS，KERNEL_DS，分别代表用户空间和内核空间，默认情况下，kernel取值为USER_DS
	set_fs(KERNEL_DS);
		
	//文件内的读位置
	r_pos = ppn * 4096 + offset; 
	printk("file_read:read position: page_num: %u\n", ppn);
	
	//移动文件指针，指示读取位置
	//llseek(struct file *filp, loff_t offset, int origin)
	//offset：偏移量；origin：偏移量相对位置，0表示绝对位置，1表示相对当前位置
	//filp->f_op->llseek(filp, offset, 0);
	filp->f_op->llseek(filp, r_pos, 0);
	
	filp->f_op->read(filp, r_buf, size, &(filp->f_pos));  
	printk("file_read:read file: %zu\n", size);	

	//改变kernel对内存地址检查的处理方式为用户空间
	set_fs(fs);
	filp_close(filp, NULL); 
	return 0;
}


/*************************************
*处理一个I/O请求，读或者写
**************************************/
static void bdev_transfer(struct my_bdev *dev, unsigned long sector,       //sector为扇区号，表示要传输的第一个扇区；nsect为该次请求所要传输的扇区数
                          unsigned long nsect, char *buffer, int write)    //nsect不会超过8，因为一次bdev_transfer最多传输4KB数据（一个页框）
{
	
	unsigned int lpn;
	unsigned long nbytes;
	unsigned int ppn;
	unsigned int old_ppn;
	
	struct invalidPage *invalidNode;

	printk("begin of bdev_transfer\n");
	lpn = sector / 8 ;                   //初始页号，这里假设直接映射，转换为页号。
	nbytes = nsect*KERNEL_SECTOR_SIZE;
	
	if(write) {
		old_ppn = pageInfo[lpn].ppn;       //获取旧的映射关系
		
		//获取物理页号
		ppn = allocPage();    //已经从空闲页链表中删除节点
		if( ppn == 0 )       //分配页失败（没有空闲页了）
		{
			printk("error: not enough free page.\n");
			return ;
		}
		printk("file_write: the ppn is %u (the lpn is %u).\n", ppn, lpn);
			
		file_write(buffer, nbytes, ppn, 0);           
		
		//更新映射表
		pageInfo[lpn].ppn = ppn;
		//写回映射表
		writeBack_mapTable();
			
		//如果是覆盖写，修改旧ppn为无效，并建立无效页节点加到无效页链表中
		if( pageInfo[old_ppn].tag == 1)     //old_ppn的标识为1说明是覆盖写
		{
			pageInfo[old_ppn].tag = 2;
			invalidNode = (struct invalidPage*)kmalloc(sizeof(struct invalidPage), GFP_KERNEL);  
			invalidNode->ppn = old_ppn;
			//将节点移到无效页链表末尾
			list_add_tail( &(invalidNode->list), &(invalid_head.list) );
			printk("now page %u is invalid.\n", old_ppn);
		}
			 
		//修改新ppn为有效
		pageInfo[ppn].tag = 1;
		//写回页信息表
		writeBack_pageTable();
		
		printk("bdev_transfer:write file: %lu\n", nbytes); 
	}
	else {
		//从映射表获取lpn对应的物理页号
		ppn = pageInfo[lpn].ppn;
		printk("file_read: the ppn is %u (lpn is %u).\n", ppn, lpn);
			
		file_read(buffer, nbytes, ppn, 0);    
		printk("bdev_transfer:read file: %lu\n", nbytes);   
	}
	printk("end of bdev_transfer\n");
}

	
/***********************************************************
*该函数遍历bio结构中的每个段，获得内存虚拟地址以访问缓冲区，
*然后调用bdev_transfer函数，已完成数据的拷贝
************************************************************/
static int bdev_xfer_bio(struct my_bdev *dev, struct bio *bio)
{
	int i;
	struct bio_vec *bvec;
	sector_t sector = bio->bi_sector;   //bi_sector该bio结构所要传输的第一个扇区
	
	printk("mode 1: begin of xfer_bio\n");
	//对每个段独立操作
	bio_for_each_segment(bvec, bio, i) {
		//为buffer返回内核虚拟地址
		char *buffer = __bio_kmap_atomic(bio, i, KM_USER0);
		
		printk("mode 1 in a xfer_bio:bdev_transfer some sectors\n");    ////////////
		bdev_transfer(dev, sector, bio_cur_bytes(bio) >> 9, 
		              buffer, bio_data_dir(bio) == WRITE);
		sector += bio_cur_bytes(bio) >> 9;
		
		//取消缓冲区映射
		__bio_kunmap_atomic(bio, KM_USER0);
	}
	printk("mode 1:end of xfer_bio\n");
	return 0;    //总是返回成功
}

//=================================================================================================
//request_mode为2时，请求队列的处理
//没有请求队列，对bio逐个处理。

/*********************************************************************************************
*当不使用请求队列时，即设备处于“无队列”模式工作，request_mode为2，驱动程序将使用make_request。
*虽然从不拥有一个请求，但是依然提供一个请求队列，make_request函数的主要参数是bio结构，
*表示要被传输的一个或多个缓冲区。该函数功能完成：直接进行传输，或者把请求重定向给其他设备。
***********************************************************************************************/
static void bdev_make_request(request_queue_t *q, struct bio *bio)
{
	struct my_bdev *dev = q->queuedata;
	int status;
	
	printk("mode 2: begin of make_request\n"); 
	printk("mode 2 in a make_requset:xfer_bio some sectors\n");    ////////////
	
	//利用前面的方法通过bio进行传输
	status = bdev_xfer_bio(dev, bio);
	//由于没有request结构进行操作，因此函数需要能够调用bio_endio，告诉bio结构的创建者请求的完成情况
	bio_endio(bio, status);
	
	printk("mode 2: end of make_request\n"); 
	return;
}

//=========================================================================================================
//块设备操作，为了模拟移动介质，bdev必须知道最后一个用户何时关闭了设备。
//驱动程序维护了一个用户计数，open和close的一个任务就是更新用户计数。

//注意新的linux内核的open、release和ioctl等函数的参数发生了变化（参照blkdev.h）
static int bdev_open(struct block_device *device, fmode_t mode)
{
	//当一个inode指向一个块设备时，i_bdev->bd_disk成员包含了指向相应gendisk结构的指针
	//该指针可用于获得驱动程序内部的数据结构
	struct my_bdev *dev;
	
	printk("begin of open\n");
	dev = device->bd_disk->private_data;
	
	//如果上次关闭时设置了“介质移除”定时器，而在30秒内，你又使用了该设备，就删除定时器
	del_timer_sync(&dev->timer);
	
	spin_lock(&dev->lock);            
	
	//查看是否更换了介质
	if( ! dev->users )
		check_disk_change(device);
	dev->users++;
	spin_unlock(&dev->lock);
	printk("end of open\n");
	return 0;
}

//减少用户计数，并启动介质移除定时器
static int bdev_release(struct gendisk *disk, fmode_t mode)
{
	
	struct my_bdev *dev;
	dev = disk->private_data;
	
	printk("begin of release\n");
	
	spin_lock(&dev->lock);
	dev->users--;
	
	if( ! dev->users ) {
		dev->timer.expires = jiffies + INVALIDATE_DELAY;  //30秒的定时器
		//add_timer(&dev->timer);
	}
	spin_unlock(&dev->lock);
	printk("end of release\n");
	printk("===================end of release=============================\n");
	return 0;
}

//调用media_changed函数以检查介质是否被改变
int bdev_media_changed(struct gendisk *gd)
{
	struct my_bdev *dev;
	
	printk("begin of media_changed\n");
	dev = gd->private_data;
	
	printk("end of media_changed\n");
	return dev->media_change;
}

//在介质改变后将调用revalidate函数，为了让驱动程序能操作新的介质，该函数要完成所有必需的工作。
//调用此函数内核将试着重新读取分区表，在这里这个函数只是简单地重置了media_change的标志位，并清除内存空间以模拟插入一张新磁盘
int bdev_revalidate(struct gendisk *gd)
{
	struct my_bdev *dev;
	
	printk("begin of revalidate\n");
	dev = gd->private_data;
	
	if(dev->media_change) {
		dev->media_change = 0;
		memset(dev->data, 0, dev->size);
	}
	
	printk("end of revalidate\n");
	return 0;
}

/*
 * The "invalidate" function runs out of the device timer; it sets
 * a flag to simulate the removal of the media.
 */
void bdev_invalidate(unsigned long ldev)
{	
	struct my_bdev *dev;
	
	printk("begin of invalidate\n");
	dev = (struct my_bdev *) ldev;
 
    spin_lock(&dev->lock);
    if (dev->users || !dev->data) 
        printk (KERN_WARNING "bdev: timer sanity check failed\n");
    else
        dev->media_change = 1;
    spin_unlock(&dev->lock);
	
	printk("end of invalidate\n");
}

//块设备驱动程序提供了ioctl函数执行设备的控制功能。
//ioctl函数在这里只处理了一个命令，对设备物理信息的查询请求
int bdev_ioctl(struct block_device *device, fmode_t mode, 
               unsigned int cmd, unsigned long arg)
{
	long size;
	struct hd_geometry geo;
	struct my_bdev *dev = NULL;
	dev = device->bd_disk->private_data;
	
	printk("begin of ioctl\n");
	switch(cmd) {
		case HDIO_GETGEO:
			//获得物理信息：由于是虚拟设备，因此不得不提供一些虚拟的信息。
			//因此这里声明有16个扇区，4个磁头，并且计算相应的柱面数。
			//这里，我们设置数据开始的位置在第四扇区
			size = dev->size * (hardsect_size / KERNEL_SECTOR_SIZE);
			geo.cylinders = (size & ~0x3f) >> 6;
			geo.heads = 4;
			geo.sectors = 16;
			geo.start = 4;
			if( copy_to_user( (void __user *)arg, &geo, sizeof(geo) ) )
				return -EFAULT;
			printk("end of ioctl\n");
			return 0;	
	}
	
	printk("end of ioctl\n");
	return -ENOTTY;    //未知命令
	
}

//设备操作的数据结构
static struct block_device_operations bdev_ops = {
	.owner              = THIS_MODULE,
	.open               = bdev_open,
	.release            = bdev_release,
	.media_changed      = bdev_media_changed,
	.revalidate_disk    = bdev_revalidate,
	.ioctl              = bdev_ioctl,
};

//===============================================================================================
//初始化函数

//初始化my_bdev结构
static void setup_device(struct my_bdev *dev, int which)
{
	printk("begin of setup_device\n");
	printk("setup_device: memset\n");
	//申请分配内存
	memset(dev, 0, sizeof(struct my_bdev));
	dev->size = nsectors * hardsect_size;
	
	printk("setup_device: dev->data = vmalloc\n");
	//kmalloc对应于kfree，可以分配连续的物理内存；
	//vmalloc对应于vfree，分配连续的虚拟内存，但是物理上不一定连续。
	dev->data = vmalloc(dev->size);      //设备的数据空间
	if(dev->data == NULL) {
		printk(KERN_NOTICE "vmalloc failure.\n");
		return;
	}
	spin_lock_init(&dev->lock);        //初始化自旋锁
	
	//the timer which "invalidate" the device
	init_timer(&dev->timer);
	dev->timer.data = (unsigned long)dev;
	dev->timer.function = bdev_invalidate;
	
	printk("setup_device: choose the request_mode\n");
	//I/O队列使用的请求模式                                     //对应每个请求模式，建立相应的请求队列，并设置相应的请求处理函数
	switch(request_mode) {
		case RM_NOQUEUE:
			dev->queue = blk_alloc_queue(GFP_KERNEL);
			if(dev->queue == NULL)
				goto out_vfree;
			blk_queue_make_request(dev->queue, bdev_make_request);
			break;
			
		default:
			printk(KERN_NOTICE "Bad request mode %d, using simple\n", request_mode);
			           /* fall into.. */
	}
	blk_queue_logical_block_size(dev->queue, hardsect_size);   //设置扇区大小
	dev->queue->queuedata = dev;
	
	printk("setup_device: dev->gd = alloc_disk\n");
	//拥有了设备内存和请求序列，就可以分配、初始化及安装相应的gendisk结构了
	//BDEV_MINORS是每个bdev设备所支持的次设备号的数量
	dev->gd = alloc_disk(BDEV_MINORS); 
	
	if( ! dev->gd ) {
		printk(KERN_NOTICE "alloc_disk failure\n");
		goto out_vfree;
	}
	dev->gd->major = bdev_major;
	dev->gd->first_minor = which * BDEV_MINORS;
	dev->gd->fops = &bdev_ops;
	dev->gd->queue = dev->queue;
	dev->gd->private_data = dev;
	snprintf(dev->gd->disk_name, 32, "my_bdev%c",which + 'a');   // /dev中显示的名字
	
	//每个请求的大小都是扇区大小的整数倍，内核总是认为扇区大小是512字节，因此必须进行转换
	set_capacity(dev->gd, nsectors * (hardsect_size / KERNEL_SECTOR_SIZE));
	
	printk("setup_device:add_disk\n");
	//结束设置过程，add_disk一定要放在初始化设备的最后一步
	add_disk(dev->gd);
	
	printk("end of setup_device\n");
	return;
	
	out_vfree:
		if(dev->data)
			vfree(dev->data);
}

static int __init bdev_init(void)
{
	int i;
	
	printk("begin of bdev_init\n");
	//注册，动态分配主设备号
	bdev_major = register_blkdev(bdev_major, "my_bdev");
	if(bdev_major <= 0) {
		printk(KERN_WARNING "bdev: unable to get major number\n");
		return -EBUSY;
	}
	printk("bdev_init: register\n");
	printk("bdev_init: Devices = kmalloc\n");
	//Allocate the device array, and initialize each one.
	Devices = kmalloc(ndevices * sizeof(struct my_bdev), GFP_KERNEL);
	if(Devices == NULL)
		goto out_unregister;
		
	printk("bdev_init: setup_device\n");
	for(i = 0; i < ndevices; i++)
		setup_device(Devices + i, i);    //初始化my_bdev结构
	
	printk("end of bdev_init\n");
	
	//初始化表文件，写入MAX_PAGE_NUM行0
	resetTable();
	//将表载入内存
	createTable();
	//测试修改Table，并写回文件
	//testTable();
	
	return 0;
	
	out_unregister:
		printk(KERN_NOTICE "kmalloc failure.\n");
		unregister_blkdev(bdev_major, "sbd");
		return -ENOMEM;
}

static void bdev_exit(void)
{
	int i;
	
	printk("begin of bdev_exit\n");
	for(i = 0; i < ndevices; i++) {
		struct my_bdev *dev = Devices + i;
		
		del_timer_sync(&dev->timer);
		if(dev->gd) {
			del_gendisk(dev->gd);
			put_disk(dev->gd);
			printk("bdev_exit:del_gendisk:release gendisk success\n");
		}
		if(dev->queue) {
			if(request_mode == RM_NOQUEUE)
				blk_put_queue(dev->queue);
			else
				blk_cleanup_queue(dev->queue);  //删除请求队列
			printk("bdev_exit:blk_cleanup_queue:rmmod success\n");
		}
		if(dev->data) {
			vfree(dev->data);
			printk("bdev_exit:release disk data success\n");
		}
	}
	unregister_blkdev(bdev_major,"my_bdev");
	printk("bdev_exit:unregister success\n");
	kfree(Devices);
	printk("end of bdev_exit\n");
}

module_init(bdev_init);
module_exit(bdev_exit);






