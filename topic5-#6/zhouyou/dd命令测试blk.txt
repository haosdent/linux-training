blk读写的特点：
写：
	static int file_write(char *w_buf, size_t size)
		filp->f_op->llseek(filp, 0, 2);       //紧接着文件末尾写
		filp->f_op->write(filp, w_buf, size, &(filp->f_pos));
		
	static void bdev_transfer(struct my_bdev *dev, unsigned long sector,       //sector为扇区号，表示要传输的第一个扇区；nsect为该次请求所要传输的扇区数
                          unsigned long nsect, char *buffer, int write)
		unsigned long nbytes = nsect*KERNEL_SECTOR_SIZE;
		file_write(buffer, nbytes);
读：
	static int file_read(char *r_buf, size_t size, loff_t offset)
		filp->f_op->llseek(filp, offset, 0);
		filp->f_op->read(filp, r_buf, size, &(filp->f_pos));
		
	static void bdev_transfer(struct my_bdev *dev, unsigned long sector,       //sector为扇区号，表示要传输的第一个扇区；nsect为该次请求所要传输的扇区数
                          unsigned long nsect, char *buffer, int write)		
		unsigned long nbytes = nsect*KERNEL_SECTOR_SIZE;       //nsect为该次请求所要传输的扇区数
		unsigned long offset = sector*KERNEL_SECTOR_SIZE;      //sector为扇区号，表示要传输的第一个扇区
		file_read(buffer, nbytes, offset);

第一种写测试：
1.  dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=9
	9个sector的写引发了两个写请求，每次请求写一页，此时文件：
	File: ‘/tmp/blkdev_file’
	Size: 8192            Blocks: 16         IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 3208911     Links: 1
	
	8192表示文件大小2个页，16表示占了16个块（每块512B）。
	
2.  紧接着
	dd if=/dev/urandom of=/dev/my_bdeva bs=512 count=1
	写了一页，此时文件：
	File: ‘/tmp/blkdev_file’
	Size: 12288            Blocks: 128         IO Block: 4096   regular file
	Device: 804h/2052d     Inode: 3208911     Links: 1

	12288表示文件大小3个页，正常。128表示占了128个块（每块512B）。
	
	
===============================================================
1.dd if=/dev/urandom of=/dev/my_bdev bs=512 count=9
===============================================================
[ 4327.882995] begin of open
[ 4327.883068] begin of media_changed
[ 4327.883070] end of media_changed
[ 4327.883072] end of open
[ 4327.883392] mode 2: begin of make_request
[ 4327.883395] mode 2 in a make_requset:xfer_bio some sectors
[ 4327.883396] mode 1: begin of xfer_bio
[ 4327.883398] mode 1 in a xfer_bio:bdev_transfer some sectors
[ 4327.883399] begin of bdev_transfer
[ 4327.883647] read file
[ 4327.883651] bdev_transfer:read file: 4096
[ 4327.883652] end of bdev_transfer
[ 4327.883654] mode 1:end of xfer_bio
[ 4327.883656] mode 2: end of make_request
[ 4327.884544] mode 2: begin of make_request
[ 4327.884547] mode 2 in a make_requset:xfer_bio some sectors
[ 4327.884548] mode 1: begin of xfer_bio
[ 4327.884549] mode 1 in a xfer_bio:bdev_transfer some sectors
[ 4327.884551] begin of bdev_transfer
[ 4327.884566] read file
[ 4327.884569] bdev_transfer:read file: 4096
[ 4327.884570] end of bdev_transfer
[ 4327.884572] mode 1:end of xfer_bio
[ 4327.884574] mode 2: end of make_request
[ 4327.884594] mode 2: begin of make_request
[ 4327.884596] mode 2 in a make_requset:xfer_bio some sectors
[ 4327.884597] mode 1: begin of xfer_bio
[ 4327.884599] mode 1 in a xfer_bio:bdev_transfer some sectors
[ 4327.884600] begin of bdev_transfer
[ 4327.884648] file_write:write file: 4096
[ 4327.884651] bdev_transfer:write file: 4096
[ 4327.884652] end of bdev_transfer
[ 4327.884654] mode 1:end of xfer_bio
[ 4327.884662] mode 2: end of make_request
[ 4327.884666] mode 2: begin of make_request
[ 4327.884668] mode 2 in a make_requset:xfer_bio some sectors
[ 4327.884669] mode 1: begin of xfer_bio
[ 4327.884671] mode 1 in a xfer_bio:bdev_transfer some sectors
[ 4327.884672] begin of bdev_transfer
[ 4327.884685] file_write:write file: 4096
[ 4327.884687] bdev_transfer:write file: 4096
[ 4327.884689] end of bdev_transfer
[ 4327.884690] mode 1:end of xfer_bio
[ 4327.884692] mode 2: end of make_request
[ 4327.884711] begin of release
[ 4327.884712] end of release
[ 4327.884782] ===================end of release=============================

=====================================================================
2.dd if=/dev/urandom of=/dev/my_bdev bs=512 count=1
=====================================================================
[ 4513.333152] begin of open
[ 4513.333160] begin of media_changed
[ 4513.333162] end of media_changed
[ 4513.333163] end of open
[ 4513.333302] mode 2: begin of make_request
[ 4513.333304] mode 2 in a make_requset:xfer_bio some sectors
[ 4513.333306] mode 1: begin of xfer_bio
[ 4513.333307] mode 1 in a xfer_bio:bdev_transfer some sectors
[ 4513.333309] begin of bdev_transfer
[ 4513.333327] read file
[ 4513.333329] bdev_transfer:read file: 4096
[ 4513.333331] end of bdev_transfer
[ 4513.333332] mode 1:end of xfer_bio
[ 4513.333335] mode 2: end of make_request
[ 4513.333362] mode 2: begin of make_request
[ 4513.333364] mode 2 in a make_requset:xfer_bio some sectors
[ 4513.333365] mode 1: begin of xfer_bio
[ 4513.333367] mode 1 in a xfer_bio:bdev_transfer some sectors
[ 4513.333368] begin of bdev_transfer
[ 4513.333441] file_write:write file: 4096
[ 4513.333444] bdev_transfer:write file: 4096
[ 4513.333445] end of bdev_transfer
[ 4513.333447] mode 1:end of xfer_bio
[ 4513.333451] mode 2: end of make_request
[ 4513.333463] begin of release
[ 4513.333465] end of release
[ 4513.333467] ===================end of release=============================

第二种写测试（先删除文件）：
1.	dd if=/dev/urandom of=/dev/my_bdev bs=1M count=1
  这次生成的文件情况：
  File: ‘/tmp/blkdev_file’
  Size: 1048576         Blocks: 2048       IO Block: 4096   regular file
  Device: 804h/2052d      Inode: 3208911     Links: 1

  bs=1M 表示一次请求写入了256个页。
  1048576表示文件大小1024K，256个页。2048表示占用2048个块（每块512B）。

2.删除文件后   dd if=/dev/urandom of=/dev/my_bdev bs=1 count=1
    生成的文件：
    File: ‘/tmp/blkdev_file’
	Size: 4096            Blocks: 8          IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 3208911     Links: 1

	虽然向设备写入的是1byte，但是文件大小仍为1个页。

	在此基础上继续dd if=/dev/urandom of=/dev/my_bdev bs=1 count=1
	File: ‘/tmp/blkdev_file’
	Size: 8192            Blocks: 128        IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 3208911     Links: 1
	
	两个页
	
	在此基础上继续dd if=/dev/urandom of=/dev/my_bdev bs=1 count=1
	File: ‘/tmp/blkdev_file’
	Size: 12288           Blocks: 128        IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 3208911     Links: 1
	
	三个页
	
	在此基础上继续dd if=/dev/urandom of=/dev/my_bdev bs=1 count=1
	File: ‘/tmp/blkdev_file’
	Size: 16384           Blocks: 128        IO Block: 4096   regular file
	Device: 804h/2052d      Inode: 3208911     Links: 1
	
	四个页


第一种读测试：
1.  echo "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba" > /dev/my_bdeva
2.  cat /tmp/blkdev_file 
	显示如下：
	abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba
	c??g?g??g??
    
	不知道为什么会有第二行，而且是乱码？
	猜测：这个是由于字符串没有填满一个页，乱码为文件系统自动填写的“空”。
	
第二种读测试：
先删除文件，待重新生成。
1.  touch a
    echo "abc" > a

2.  dd if=a of=/dev/my_bdeva  bs=1 count=1
    (cat /tmp/blkdev_file)
	文件内容（一页大小）：
	a
	
3.  dd if=a of=/dev/my_bdeva  bs=1 count=1 skip=1
	文件内容（两页大小，因为这一次跟上一次算作两次写请求）：
	ab
	
	skip参数：表明skip跳跃是指if里面的起始读位置，以bs=1B为单位
	
4.  dd if=a of=/dev/my_bdeva  bs=1 count=1 skip=2
	文件内容（三页大小）：
	abc
	
5.  touch b

6.  dd if=/tmp/blkdev_file of=b bs=1 count=1
	b文件中内容为
	a

7.  dd if=/tmp/blkdev_file of=b bs=1 count=1 seek=1
	b文件中内容为
    aa	
	
8.  dd if=/tmp/blkdev_file of=b bs=1 count=1
	b文件中内容为
    a
	
	seek参数：指示of里面的写位置，以bs=1B为单位

9.  dd if=/tmp/blkdev_file of=b bs=1 count=1 skip=1
	b文件中内容为空（因为file中第一页的内容只有一个a，只使用了一个字符长度）
	
10. dd if=/tmp/blkdev_file of=b bs=1 count=1 skip=4096
	b文件中内容为
	b
	
	表明读到了blkdev_file文件中第二页的内容，说明了该文件的组织形式。

11. dd if=/dev/my_bdeva of=b bs=1 count=1
	b文件中内容为
	a
	
	dd if=/dev/my_bdeva of=b bs=1 count=1 skip=4096
	b文件中内容为
	b
	
	表明块设备文件跟blkdev_file文件的存储组织方式一样。
