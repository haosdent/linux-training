#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/init.h>
#include <linux/ip.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <net/sock.h>
#include <net/netlink.h>

MODULE_LICENSE("GPL");

struct sock *mynl_sk = NULL;

void sendnlmsg(char *message, int dstPID)
{
	struct sk_buff *myskb;
    struct nlmsghdr *mynlh;
    int len = NLMSG_SPACE(1024);
	int slen = 0;

    if(!message || !mynl_sk){
        return;
    }
    
    myskb = alloc_skb(len, GFP_KERNEL);
    if(!myskb){
        printk(KERN_ERR "my_net_link: alloc_skb Error./n");
        return;
    }
	
    slen = strlen(message)+1;
    mynlh = nlmsg_put(myskb, 0, 0, 0, (NLMSG_SPACE(1024)-sizeof(struct nlmsghdr)), 0);

    NETLINK_CB(myskb).pid = 0; // 消息发送者的id标识，如果是内核发的则置0
    NETLINK_CB(myskb).dst_group = 0; //如果目的组为内核或某一进程，该字段也置0

	message[slen] = '\0';
    memcpy(NLMSG_DATA(mynlh), message, slen+1);

    //通过netlink_unicast()将消息发送用户空间由dstPID所指定了进程号的进程
    netlink_unicast(mynl_sk,myskb,dstPID,0);
	
    printk("send OK!\n");
    return;
}

static void mynl_data_ready (struct sk_buff *__skb)
{
	struct sk_buff *myskb;
	struct nlmsghdr *mynlh = NULL;
	
	myskb = skb_get(__skb);
	mynlh = (struct nlmsghdr *)myskb->data;
	printk("%s: received netlink message payload: %s \n", __FUNCTION__, (char*)NLMSG_DATA(mynlh));
	kfree_skb(myskb);
	sendnlmsg("I see you",mynlh->nlmsg_pid);
	printk("recvied finished!\n");
}

static int myinit_module(void)
{
	printk("my netlink in!\n");
	mynl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, 0, mynl_data_ready, NULL, THIS_MODULE);
	return 0;
}

static void myexit_module(void)
{
	printk("my netlink out!\n");
	sock_release(mynl_sk->sk_socket);
}

module_init(myinit_module);
module_exit(myexit_module);