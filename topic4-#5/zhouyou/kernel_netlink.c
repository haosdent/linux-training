#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/init.h>
#include <linux/ip.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <net/sock.h>
#include <linux/netlink.h>

#define NETLINK_TEST 22
#define MAX_MSGSIZE 1024

MODULE_AUTHOR("ZY");
MODULE_LICENSE("Dual BSD/GPL");

//sock结构是linux网络体系的核心数据结构，socket结构的一个成员
//sock实际是socket的一个内核表示数据结构，用户态应用创建的socket在内核中也会有一个sock结构来表示
struct sock *nl_sk = NULL;

//向用户空间发送消息的接口
static void sendnlmsg(char *message, int dstPID)
{
    struct sk_buff *skb;
    struct nlmsghdr *nlh;
    int len = NLMSG_SPACE(MAX_MSGSIZE);
    int slen = 0;
	
	/*
	
	if(!message || !nl_sk){
		printk(KERN_ERR "my_netlink: NULL message or nl_sk.\n");
		return;
	}
	*/
	
	//为新的sk_buffer申请空间
    skb = alloc_skb(len, GFP_KERNEL);//
	if(!skb){
		printk(KERN_ERR "my_netlink: alloc_skb error.\n");
		return;
	}
		
    //slen = strlen(message) + 1;
    slen = strlen(message);
		
	//设置netlink消息头部
    nlh = nlmsg_put(skb, 0, 0, 0, MAX_MSGSIZE, 0);
	
    //设置netlink的控制块
	//消息发送者的id标识，内核发的则置为0
    //NETLINK_CB(skb).pid = 0;  
	NETLINK_CB(skb).portid = 0;  
	//目的组为内核或某一进程，则该字段置为0	
    NETLINK_CB(skb).dst_group = 0;         
        
    message[slen] = '\0';
    memcpy(NLMSG_DATA(nlh), message, slen + 1);
	
    //通过netlink_unicast()将消息发送用户空间由dstPID所指定了进程号的进程
    netlink_unicast(nl_sk, skb, dstPID, 0);
    printk("send OK!\n");
    return;
}

static void nl_data_ready(struct sk_buff *__skb)
{
        struct sk_buff *skb = skb_get(__skb);
        struct nlmsghdr *nlh = NULL;
        nlh = nlmsg_hdr(skb);//return (struct nlmsghdr *)skb->data
		printk("%s: received netlink message: %s \n", __FUNCTION__, (char*)NLMSG_DATA(nlh));
		kfree_skb(skb);
        sendnlmsg("Oh,I hear you", nlh->nlmsg_pid);
        printk("received finished \n");
}

/* 
新添加的结构：set optional configurations for netlink kernel sockets
    struct netlink_kernel_cfg {
          unsigned int    groups;
          unsigned int    flags;
          void            (*input)(struct sk_buff *skb);
          struct mutex    *cb_mutex;
          void            (*bind)(int group);
  };
*/
struct netlink_kernel_cfg cfg = {
	.input = nl_data_ready,
};

static int __init nl_init(void)
{
    printk("my netlink init\n");
    //创建内核态的socket
	//nl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, 0, nl_data_ready, NULL, THIS_MODULE);
	//改变(3.7)：netlink_kernel_create(struct net *net, int unit, struct netlink_kernel_cfg *cfg)
	nl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, &cfg);
    return 0;
}

static void __exit nl_exit(void)
{
    printk("my netlink exit\n");
    if(nl_sk)
        sock_release(nl_sk->sk_socket);
}

module_init(nl_init);
module_exit(nl_exit);





