#用法

##1. 编译 

    make

##2. 加载模块

    make install

##3. 编译用户程序

    make gcc

##4. 测试程序

    make test

##5. 卸载模块

    make uninstall

##6. 清除相关文件

    make clean

#3.7和3.8内核与之前发生了变化的接口（linux / include / linux / netlink.h）：

##1.netlink_kernel_create

    //旧接口定义：
    netlink_kernel_create (struct net *net, int unit, unsigned int groups, void (*input)(struct sk_buff *skb), struct mutex *cb_mutex, struct module *module);
    //创建接口：
    nl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, 0, nl_data_ready, NULL, THIS_MODULE);

    //新接口(3.7)定义：
    netlink_kernel_create(struct net *net, int unit, struct netlink_kernel_cfg *cfg);
    //创建接口：
    nl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, &cfg);

##2. 成员名称

    //旧名称：
    NETLINK_CB(skb).pid = 0;  

    //新名称：
    NETLINK_CB(skb).portid = 0;
