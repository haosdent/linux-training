#include <linux/init.h>   
#include <linux/module.h>   
#include <linux/timer.h> 
#include <linux/time.h>  
#include <linux/types.h>
#include <net/sock.h>
#include <net/netlink.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("CH");

#define NETLINK_TEST 22
#define MAX_TIME 10

struct sock *nl_sk = NULL;
static struct timer_list my_timer;  
struct timeval now={0,0};
unsigned long token=0;
unsigned int count=0;
unsigned int firstflag=1;
u32 pid=0;


/*get string's length*/
int stringlength(char *s)
{
    int slen=0;
    for(;*s;s++)
        slen++;
    return slen;
}
//发送netlink消息message
void sendnlmsg(char *message)  
{
    struct sk_buff *skb;
    struct nlmsghdr *nlh;
    int rc;
    int len = NLMSG_SPACE(1200);
    int slen=0;

    if(!message)
        return;
    if(!nl_sk)
        return;
    if(pid==0)
        return;
    //分配一个新的sk_buffer
    skb = alloc_skb(len, GFP_ATOMIC);
    if (!skb){
        printk(KERN_ERR "net_link: allocate failed.\n");
        return;
    }
    slen=stringlength(message);
    nlh = nlmsg_put(skb,0,0,0,1200,0);//填充数据包头
    NETLINK_CB(skb).pid = 0;      /* from kernel */
    //下面必须手动设置字符串结束标志\0，否则用户程序可能出现接收乱码
    message[slen]='\0';
    memcpy(NLMSG_DATA(nlh), message, slen+1);
    printk("net_link: going to send. size=%d\n",slen);
    //使用netlink单播函数发送消息
    rc = netlink_unicast(nl_sk, skb, pid, MSG_DONTWAIT);
    //如果发送失败，则打印警告并退出函数
    if (rc < 0) 
    {
        printk(KERN_ERR "net_link: can not unicast skb (%d)\n", rc);
        return;
    }
    printk("net_link: send is ok.\n");
}


void nl_data_ready (struct sk_buff *__skb)
{
    struct sk_buff *skb;//sk_buff为网络协议栈使用的数据结构
    struct nlmsghdr *nlh;
    char str[100];

    printk("net_link: here comes the new data...\n");
    skb = skb_get(__skb);//获取数据包

    if (skb->len >= NLMSG_SPACE(0)) {
        nlh = nlmsg_hdr(skb);
        printk("net_link: received: %s.\n", (char *)NLMSG_DATA(nlh));
        memcpy(str,NLMSG_DATA(nlh), sizeof(str)); 
        pid = nlh->nlmsg_pid; /*pid of sending process */
        printk("net_link: pid is %d\n", pid);
        kfree_skb(skb);
        if(strcmp(str,"EXIT")==0)
        {
            pid=0;
            firstflag=1;
            return;
        }
        //收到第一条消息，，反馈后启动定时器
        if(firstflag)
        {
            printk("First message received. Let's get started!\n");
            sendnlmsg(str);
            firstflag=0;
            add_timer(&my_timer); 
        }
    }
    return;
}



//定时函数
void tfunction(unsigned long arg)   
{ 
    char buff[100];
    if(count<MAX_TIME) 
    {
        token=arg;
        do_gettimeofday(&now);
        printk("Count=%2u, token=%ld, time.sec=%ld\n",count,token,now.tv_sec); 
        sprintf(buff,"Routine message. count=%d",count);
        sendnlmsg(buff);
        memset(buff,0,sizeof(buff));
        count++;
        mod_timer(&my_timer,jiffies+HZ);
    }
    else
    {
        printk("time is up, now sending EXIT...\n");
        sendnlmsg("EXIT");
    }

} 

//初始化netlink
void netlink_init(void) {
    nl_sk = netlink_kernel_create(&init_net, 
            NETLINK_TEST, 0, nl_data_ready, NULL, THIS_MODULE);

    if (!nl_sk) 
    {
        printk(KERN_ERR "net_link: Cannot create netlink socket.\n");
        pid=0;
        if (nl_sk != NULL){
            sock_release(nl_sk->sk_socket);
        }
        return;
    }
    printk("net_link: create socket ok.\n");
    return;

}

//初始化模块和定时器
static int my_init(void)   
{   
    init_timer(&my_timer);   
    my_timer.data=1000;   
    my_timer.function=tfunction;   
    my_timer.expires=jiffies+HZ;   //定时一秒钟


    do_gettimeofday(&now);
    printk(KERN_ALERT "nltimer_k module loaded, time.sec=%ld\n",now.tv_sec);   
    netlink_init();
    printk(KERN_ALERT "netlink_init executed.\n");
    return   0;   
}   
//模块退出处理
static void my_exit(void)   
{   
    del_timer(&my_timer);
    if (nl_sk != NULL){
        sock_release(nl_sk->sk_socket);
    }
    printk("nltimer_k module exited\n");   
}   

module_init(my_init);   
module_exit(my_exit);   

