#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/socket.h>

#define NETLINK_TEST 22
#define MAX_PAYLOAD 1024  /* maximum payload size*/
struct sockaddr_nl src_addr, dest_addr;
struct nlmsghdr *nlh = NULL;
struct iovec iov;
int sock_fd;
struct msghdr msg;

void sendnlmsg(char *message)
{
    nlh=(struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    /* Fill the netlink message header */
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh->nlmsg_pid = getpid();  /* self pid */
    nlh->nlmsg_flags = 0;
    /* Fill in the netlink message payload */
    strcpy(NLMSG_DATA(nlh), message);

    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    printf(" Sending message. ...\n");
    sendmsg(sock_fd, &msg, 0);
}

int main(int argc, char* argv[])
{
    char str[100];
    char buff[110];
    sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_TEST);
    memset(&msg, 0, sizeof(msg));
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();  /* self pid */
    src_addr.nl_groups = 0;  /* not in mcast groups */
    bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0;   /* For Linux Kernel */
    dest_addr.nl_groups = 0; /* unicast */

    //发送初始消息
    sendnlmsg("Hello You!");

    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    //循环接收消息，直到收到退出标志EXIT
    while(1)
    {
        /* Read message from kernel */	
        printf(" Waiting message. ...\n");
        recvmsg(sock_fd, &msg, 0);
        strcpy(str,(char *)NLMSG_DATA(nlh));
        if(strcmp(str,"EXIT")==0)
        {
            sendnlmsg("EXIT");
            printf(" Kernel Module Time's up, now we quit.\n");
            break;
        }
        else
        {
            printf(" Received message payload: %s, size=%d\n",str,strlen(str));
            strcpy(buff,"Echo->");
            sendnlmsg(strcat(buff,str));
            memset(buff,0,sizeof(buff));
        }

    }
    /* Close Netlink Socket */
    close(sock_fd);
    return 0;
}

